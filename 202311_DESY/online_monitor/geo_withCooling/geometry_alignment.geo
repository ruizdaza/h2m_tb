[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.0141521deg,0.0174752deg,0.806381deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.6093mm,102.224um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.73648deg,0.0470971deg,-0.0768336deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 405.323um,500.748um,108mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -2.18337deg,-0.273988deg,0.149542deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 814.507um,110.823um,135mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -600um,0um,175mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,202mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.0223454deg,0.110065deg,0.0506495deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 727.943um,96.184um,335mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.338102deg,-6.08745deg,0.0470398deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.43189mm,-56.54um,400mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

