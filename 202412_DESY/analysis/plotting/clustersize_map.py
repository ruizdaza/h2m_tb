import ROOT
import numpy as np
import matplotlib.pyplot as plt

# Load the ROOT file
file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202412/h2m10_1V2_toa_ikrum21_thr107.root"
root_file = ROOT.TFile.Open(file_path)

if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

tprofile_path = "AnalysisDUT/H2M_0/clusterSize_trackPos_TProfile_4p"
tprofile = root_file.Get(tprofile_path)


if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Create arrays to store x, y, and z values
x_values = np.array([tprofile.GetXaxis().GetBinCenter(i) for i in range(1, nx + 1)])
y_values = np.array([tprofile.GetYaxis().GetBinCenter(j) for j in range(1, ny + 1)])
z_values = np.array([[tprofile.GetBinContent(j, i) for i in range(1, nx + 1)] for j in range(1, ny + 1)])

# Combine the bins
factor = 5# factor of 2 is 35 bins/pixel. factor of 10 is 7 bins/pixel. factor 7 is 10 bins/pixel

# Combine
x_values_combined = x_values.reshape(-1, factor).mean(axis=1)
y_values_combined = y_values.reshape(-1, factor).mean(axis=1)
z_values_combined = z_values.reshape(ny // factor, factor, nx // factor, factor).mean(axis=(1, 3))

# Plotting
plt.figure(figsize=(8, 8))
img = plt.imshow(z_values_combined.T, extent=(x_values_combined.min(), x_values_combined.max(),
                                              y_values_combined.min(), y_values_combined.max()),
                 origin='lower', cmap='viridis')
cbar = plt.colorbar(img, label='Cluster size [px]', shrink=0.82)

cbar.set_label('Cluster size [px]', fontsize=20)
img.set_clim(1,1.5)

cbar.ax.tick_params(axis='y', labelsize=20)

plt.xlabel(r'in-pixel $x_{\mathrm{track}}$ [$\mu$m]', fontsize=20)
plt.ylabel(r'in-pixel $y_{\mathrm{track}}$ [$\mu$m]', fontsize=20)

# Add grey discontinuous lines at x=35 and y=35
plt.axvline(x=35, color='grey', linestyle='--')
plt.axhline(y=35, color='grey', linestyle='--')
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)

output_image_path = "output_clustersize_h2m10_1V2_toa_ikrum21_thr107.pdf"
plt.savefig(output_image_path)
plt.show()
