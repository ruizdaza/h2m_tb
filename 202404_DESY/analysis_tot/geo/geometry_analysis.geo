[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.15233deg,0.747481deg,0.769883deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.04516mm,-316.108um,0um
roi = [[350,240],[350,380],[560,380],[560,240]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.25867deg,-1.15182deg,-0.0527694deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -89.989um,2.095um,24mm
roi = [[350,240],[350,380],[560,380],[560,240]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.90892deg,-0.631056deg,0.174351deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 182.457um,-436.717um,51mm
roi = [[350,240],[350,380],[560,380],[560,240]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
mask_file = "/scratch/ruizdaza/h2m/h2m_tb/202404_DESY/analysis_tot/geo/masking.txt"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = -0.0126624deg,0.0273874deg,0.0780369deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -509.674um,1.14621mm,96mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
roi = [[350,240],[350,380],[560,380],[560,240]]
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.04066deg,-0.527809deg,0.137567deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 607.573um,91.895um,149mm
roi = [[350,240],[350,380],[560,380],[560,240]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.04158deg,0.541502deg,0.18031deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 625.015um,-59.173um,175mm
roi = [[350,240],[350,380],[560,380],[560,240]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

