[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.0343775deg,-0.0376433deg,0.790395deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.37714mm,62.053um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.688351deg,-0.591865deg,-0.0620513deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 190.837um,435.76um,105mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.93608deg,-0.483576deg,0.167762deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 604.553um,39.422um,132mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
mask_file = "/home/teleuser/h2m/tb_desy_202311/masking.txt"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.250956deg,-0.308996deg,-0.599486deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -712.468um,223.543um,172mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,194mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.242648deg,-0.789536deg,0.150459deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 720.034um,90.676um,221mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.49515deg,-1.74815deg,0.159569deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.40182mm,-71.562um,325mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

