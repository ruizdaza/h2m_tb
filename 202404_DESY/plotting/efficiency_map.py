import ROOT
import numpy as np
import matplotlib.pyplot as plt

# Load the ROOT file
#file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern_secondtest/output_130517/output_130517/dut_analysis.root"
file_path = "/scratch/ruizdaza/h2m/h2m_tb/202404_DESY/analysis_tot/output_thr106.root"

root_file = ROOT.TFile.Open(file_path)

# Check if the file is opened successfully
if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

# Navigate to the TProfile path
tprofile_path = "AnalysisEfficiency/H2M_0/pixelEfficiencyMap4pixels_trackPos_TProfile"
tprofile = root_file.Get(tprofile_path)

# Check if the TProfile is found
if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

# Get the TProfile data
nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Create arrays to store x, y, and z values
x_values = np.array([tprofile.GetXaxis().GetBinCenter(i) for i in range(1, nx + 1)])
y_values = np.array([tprofile.GetYaxis().GetBinCenter(j) for j in range(1, ny + 1)])
z_values = np.array([[tprofile.GetBinContent(j, i) for i in range(1, nx + 1)] for j in range(1, ny + 1)])

# Create a heatmap using matplotlib
plt.figure(figsize=(8, 8))
img = plt.imshow(z_values.T, extent=(x_values.min(), x_values.max(), y_values.min(), y_values.max()), origin='lower', cmap='viridis')
cbar = plt.colorbar(img, label='Efficiency', shrink=0.82)  # Set shrink to control colorbar size

# Manually adjust the font size of the colorbar label
cbar.set_label('Efficiency', fontsize=16)
#img.set_clim(0.45, 1)

# Set font size for colorbar tick labels
cbar.ax.tick_params(axis='y', labelsize=16)

# Set axis labels and title with increased label size
plt.xlabel(r'in-pixel $x_{\mathrm{track}}$ [$\mu$m]', fontsize=16)
plt.ylabel(r'in-pixel $y_{\mathrm{track}}$ [$\mu$m]', fontsize=16)

# Add grey discontinuous lines at x=35 and y=35
plt.axvline(x=35, color='grey', linestyle='--')
plt.axhline(y=35, color='grey', linestyle='--')

# Save the plot
output_image_path = "output_effmap_matplotlib.pdf"
plt.savefig(output_image_path)

# Show the plot
plt.show()
