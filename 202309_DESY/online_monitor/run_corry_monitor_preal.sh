#!/usr/bin/bash
datapath=/data3/202309_h2m/data/
corryconfig=/data3/202309_h2m/macros/onlinemonitor_preal.conf

run=${1}

latest_telescope=$(find $datapath -name "*${run}_tel*" -print0 | xargs -r -0 ls -1 -t | head -n1)
#latest_h2m=$(find $datapath -name "*h2m*" -print0 | xargs -r -0 ls -1 -t | head -n1)

telescope_run=$(echo $latest_telescope | grep -o "run[0-9]*")
h2m_run=$(echo $latest_h2m | grep -o "run[0-9]*")

#if [[ $telescope_run != $apts_run ]]; then
#    echo "$latest_telescope and $latest_h2m seem to be from different runs" ;
#else
    echo "*********************************************"
    echo "* opening $latest_telescope and $latest_h2m"
    echo "*********************************************"

/home/teleuser/software/corryvreckan/bin/corry -c ${corryconfig} \
		   -o histogram_file=histograms_${h2m_run}_online.root \
                   -o EventLoaderEUDAQ2:TLU_0.file_name=${latest_telescope} \
		   -o EventLoaderEUDAQ2.file_name=${latest_telescope}
