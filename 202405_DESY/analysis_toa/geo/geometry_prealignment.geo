[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.39431deg,182.266deg,-0.643546deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.26584mm,83.424um,8mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.273014deg,180.143deg,0.159855deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -205.375um,517.292um,156mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.63029deg,179.833deg,-0.0892668deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -577.018um,129.05um,183mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
mask_file = "/scratch/ruizdaza/h2m/h2m_tb/202405_DESY/analysis_toa/geo/masking_1V2.txt"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 180.769deg,1.50224deg,89.6001deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 988.086um,-597.029um,222mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,246mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.75831deg,179.015deg,-0.0714478deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -643.034um,80.368um,273mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.49594deg,181.82deg,-0.109836deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.07133mm,-90.273um,423mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

