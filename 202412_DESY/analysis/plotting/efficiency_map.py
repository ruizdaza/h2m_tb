import ROOT
import numpy as np
import matplotlib.pyplot as plt

#Open ROOT file

#file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/h2m8_1V2_ikrum21_tot.root"
file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202412/h2m10_1V2_tot_ikrum10_thr105.root"

root_file = ROOT.TFile.Open(file_path)
if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

tprofile_path = "AnalysisEfficiency/H2M_0/pixelEfficiencyMap4pixels_trackPos_TProfile"
tprofile = root_file.Get(tprofile_path)

if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

# Rebin. The bin size is 0.5 um/bin. So (35,35) makes 10 bins/pixel.

tprofile.Rebin2D(25,25)

# Get the TProfile data
nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Create arrays to store x, y, and z values
x_values = np.array([tprofile.GetXaxis().GetBinCenter(i) for i in range(1, nx + 1)])
y_values = np.array([tprofile.GetYaxis().GetBinCenter(j) for j in range(1, ny + 1)])
z_values = np.array([[tprofile.GetBinContent(j, i) for i in range(1, nx + 1)] for j in range(1, ny + 1)])

# Plotting
plt.figure(figsize=(8, 8))
img = plt.imshow(z_values.T, extent=(x_values.min(), x_values.max(),
                                              y_values.min(), y_values.max()),
                 origin='lower', cmap='viridis', vmin=0.7, vmax=1)
cbar = plt.colorbar(img, label='Efficiency', shrink=0.82)

cbar.set_label('Efficiency', fontsize=20)

cbar.ax.tick_params(axis='y', labelsize=20)

plt.xticks(fontsize=16)
plt.yticks(fontsize=16)

plt.xlabel(r'in-pixel $x_{\mathrm{track}}$ [$\mu$m]', fontsize=20)
plt.ylabel(r'in-pixel $y_{\mathrm{track}}$ [$\mu$m]', fontsize=20)

# Add grey discontinuous lines at x=35 and y=35
plt.axvline(x=35, color='grey', linestyle='--')
plt.axhline(y=35, color='grey', linestyle='--')


output_image_path = "output_efficiency_map_h2m10_1V2_tot_ikrum10_thr105.pdf"
plt.savefig(output_image_path)
plt.show()
