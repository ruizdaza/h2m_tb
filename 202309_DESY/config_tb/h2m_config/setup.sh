# IP variables for start scripts
export RUNCONTROLIP=192.168.22.1

# eudaq2 binaries
export EUDAQ=~/software/eudaq/
export PATH=$PATH:$EUDAQ/bin

## for AIDA TLU
# for Ubuntu 18.04
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/cactus/lib
# start controlhub for stable AIDA TLU TCP/IP communication
/opt/cactus/bin/controlhub_start
/opt/cactus/bin/controlhub_status
