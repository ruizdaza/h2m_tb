[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.882011deg,0.196467deg,0.768566deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0mm,0um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.442152deg,-0.3852deg,-0.0632545deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,24mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.73669deg,-0.523454deg,0.177216deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,51mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

#[H2M_0]
#coordinates = "cartesian"
#mask_file = "/home/teleuser/h2m/tb_desy_202311/masking.txt"
#material_budget = 0.00075
#number_of_pixels = 64, 16
#orientation = 0.250956deg,-0.308996deg,-0.599486deg
#orientation_mode = "xyz"
#pixel_pitch = 35um,35um
#position = 0um,0um,96mm
#role = "dut"
#spatial_resolution = 9um,9um
#time_resolution = 1ns
#type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.035638deg,-1.28772deg,0.141177deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,149mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.021887deg,-2.15243deg,0.174867deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0mm,0um,175mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"


