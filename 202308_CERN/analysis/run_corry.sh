#!/bin/bash

RUN=${1}

datapath=/pnfs/desy.de/ftx-tb/202308_CERNTB_H2M


INPUTFILE_H2M=$(find $datapath -name "run*${RUN}_*" )
INPUTDIR_TPX=/pnfs/desy.de/ftx-tb/202308_CERNTB_H2M/data/Run${RUN}

echo "--- Input H2M: "  ${INPUTFILE_H2M}
echo "--- Input tpx3: " ${INPUTDIR_TPX}

#/scratch/ruizdaza/corryvreckan/bin/corry -c 2_prealignment.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} &&
#/scratch/ruizdaza/corryvreckan/bin/corry -c 3_tracking_after_prealignment.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} &&
#/scratch/ruizdaza/corryvreckan/bin/corry -c 4_alignment.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} &&
#/scratch/ruizdaza/corryvreckan/bin/corry -c 5_first_dut_residuals.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 6_dut_alignment_1.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 6_dut_alignment_2.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 6_dut_alignment_3.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 7_dut_analysis.conf -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX}
