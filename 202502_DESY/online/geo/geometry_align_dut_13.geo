[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.11419deg,181.711deg,-0.395054deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -220.575um,314.772um,8mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.27821deg,180.563deg,0.225631deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 719.091um,689.823um,173mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 3.15241deg,180.261deg,-0.00939651deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 548.239um,381.074um,198mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 186.009deg,-1.23742deg,90.1836deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 455.552um,2.28492mm,276mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,378mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.489707deg,179.492deg,0.0291636deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 143.734um,245.797um,404mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.08724deg,179.756deg,0.0771774deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 568.401um,91.132um,430mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

