[W0013_D04]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 186.257deg,-189.361deg,-0.549696deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 565.285um,247.079um,0um
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_E03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189.391deg,-188.945deg,-0.5085deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -140.723um,173.27um,21mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_G02]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189deg,-189deg,0deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0um,0um,42.5mm
role = "reference"
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[H2M_0]
coordinates = "cartesian"
number_of_pixels = 64, 16
orientation = 179.998deg,-0.00154699deg,0.703019deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 375.681um,54.65um,113mm
role = "dut"
spatial_resolution = 10um,10um
time_resolution = 3ns
type = "h2m"

[W0013_G03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.394deg,-8.47032deg,-0.944234deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -35.821um,-219.144um,185.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_J05]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.384deg,-8.46305deg,-0.821564deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -139.639um,-91.577um,207.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_L09]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 186.868deg,-8.55397deg,-0.530215deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -195.294um,-163.401um,229mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"
