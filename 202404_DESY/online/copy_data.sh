#!/usr/bin/bash

run_number=${1}

# Copy telescope data
scp -pC teleuser@fhltb-rc22:/data1/tangerine/h2m_2404/data/*run000${run_number}* data/.

# Copy telepix data
#scp -pC lhuth@fhl-mupix:/run/media/lhuth/data/202404_h2m/rawData/*${run_number}* data/.
