import ROOT
import numpy as np
import matplotlib.pyplot as plt

# Load the ROOT file
file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202412/h2m10_3V6_tot_ikrum10_thr105.root"
#file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202404/h2m2_3V6_toa_ikrum21/thr109.root"
root_file = ROOT.TFile.Open(file_path)

if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

tprofile_path = "AnalysisDUT/H2M_0/qvsxmym4p"
tprofile = root_file.Get(tprofile_path)

if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Create arrays to store x, y, and z values
x_values = np.array([tprofile.GetXaxis().GetBinCenter(i) for i in range(1, nx + 1)])
y_values = np.array([tprofile.GetYaxis().GetBinCenter(j) for j in range(1, ny + 1)])
z_values = np.array([[tprofile.GetBinContent(j, i) for i in range(1, nx + 1)] for j in range(1, ny + 1)])

# Combine the bins
rebin_factor = 5# factor of 2 is 35 bins/pixel. factor of 10 is 7 bins/pixel. factor 7 is 10 bins/pixel

# Function to rebin data by averaging over neighboring bins
def rebin_data(data, factor):
    shape = (data.shape[0] // factor, factor, data.shape[1] // factor, factor)
    return data.reshape(shape).mean(axis=(1, 3))

# Apply rebinning
z_values_rebinned = rebin_data(z_values, rebin_factor)
x_values_rebinned = np.mean(x_values.reshape(-1, rebin_factor), axis=1)
y_values_rebinned = np.mean(y_values.reshape(-1, rebin_factor), axis=1)

# Plotting
plt.figure(figsize=(8, 8))
img = plt.imshow(z_values_rebinned.T, extent=(x_values_rebinned.min(), x_values_rebinned.max(),
                                              y_values_rebinned.min(), y_values_rebinned.max()),
                 origin='lower', cmap='viridis', vmin=40, vmax=60)

cbar = plt.colorbar(img, label='$t_{\mathrm{trigger}} - t_{\mathrm{hit}}$ [ns]', shrink=0.82)

cbar.set_label('Cluster charge [clock cycles]', fontsize=20)

# Set the range of the color map (clim)
#img.set_clim(60, 260)

cbar.ax.tick_params(axis='y', labelsize=20)

plt.xlabel(r'in-pixel $x_{\mathrm{track}}$ [$\mu$m]', fontsize=20)
plt.ylabel(r'in-pixel $y_{\mathrm{track}}$ [$\mu$m]', fontsize=20)

# Add grey discontinuous lines at x=35 and y=35
plt.axvline(x=35, color='grey', linestyle='--')
plt.axhline(y=35, color='grey', linestyle='--')

plt.xticks(fontsize=16)
plt.yticks(fontsize=16)

output_image_path = "output_tot_cluster_matplotlib_h2m10_3V6_tot_ikrum10_thr105.pdf"
plt.savefig(output_image_path)

plt.show()
