#include <iostream>
#include <TFile.h>
#include <TProfile2D.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TAxis.h>

void analysis_outermostPixels_row() {
    // Create a single canvas for X and Y projections, and ratios
    TCanvas* canvas = new TCanvas("ProjectionCanvas", "Projections", 1200, 600);
    TPad* padX = new TPad("padX", "X Projection", 0.0, 0.25, 0.5, 1.0);
    TPad* padY = new TPad("padY", "Y Projection", 0.5, 0.25, 1.0, 1.0);
    TPad* padXRatio = new TPad("padXRatio", "X Projection Ratio", 0.0, 0.0, 0.5, 0.27);
    TPad* padYRatio = new TPad("padYRatio", "Y Projection Ratio", 0.5, 0.0, 1.0, 0.27);

    padX->Draw();
    padY->Draw();
    padXRatio->Draw();
    padYRatio->Draw();

    // Create legends for X and Y projections
    TLegend* legendX = new TLegend(0.7, 0.7, 0.95, 0.9);
    //TLegend* legendY = new TLegend(0.6, 0.7, 0.9, 0.9);

    // Read data
    const char* fileNames[3] = {
      "/scratch/ruizdaza/h2m/h2m_tb/202311_DESY/analysis/output/dut_analysis_topRow.root",
      "/scratch/ruizdaza/h2m/h2m_tb/202311_DESY/analysis/output/dut_analysis_bottomRow.root",
      "/scratch/ruizdaza/h2m/h2m_tb/202311_DESY/analysis/output/dut_analysis_Row7.root"
    };

    TH1D* xProjections[3];
    TH1D* yProjections[3];
    TH1D* xRatio[3];
    TH1D* yRatio[3];
    const char* labels[5] = {"Top Row", "Bottom Row", "Middle Row"};    int colors[3] = {2, 3, 1}; // Line colors (1 = black)

    for (int i = 0; i < 3; ++i) {
        TFile* file = TFile::Open(fileNames[i]);
        if (!file || file->IsZombie()) {
            std::cerr << "Error: Cannot open the ROOT file " << fileNames[i] << std::endl;
            continue;  // Move to the next file if one fails
        }
        // We could read the TGraph, but TProfile is probably good as we are far from efficiency = 1 or 0.
        TProfile2D* profile2D = dynamic_cast<TProfile2D*>(file->Get("AnalysisEfficiency/H2M_0/pixelEfficiencyMap_trackPos_TProfile"));
        if (!profile2D) {
            std::cerr << "Error: Cannot retrieve the TProfile2D from " << fileNames[i] << std::endl;
            file->Close();
            continue;  // Move to the next file if one fails
        }

        TH2D* hist2D = profile2D->ProjectionXY();
        xProjections[i] = hist2D->ProjectionX();
        yProjections[i] = hist2D->ProjectionY();

        // Projection plots
        xProjections[i]->SetLineColor(colors[i]);  // Set line colors
        xProjections[i]->SetStats(0);  // Disable statistics box
        xProjections[i]->SetLineWidth(2);  // Set line width
        xProjections[i]->Scale(1.0 / 35.0);  // Divide by 35

        yProjections[i]->SetLineColor(colors[i]);  // Set line colors
        yProjections[i]->SetStats(0);  // Disable statistics box
        yProjections[i]->SetLineWidth(2);  // Set line width
        yProjections[i]->Scale(1.0 / 35.0);  // Divide by 35

        // Calculate the ratio plots
        xRatio[i] = dynamic_cast<TH1D*>(xProjections[i]->Clone(Form("xRatio_%d", i)));
        yRatio[i] = dynamic_cast<TH1D*>(yProjections[i]->Clone(Form("yRatio_%d", i)));

    }
    xRatio[0]->Divide(xProjections[2]);
    xRatio[1]->Divide(xProjections[2]);
    xRatio[2]->Divide(xProjections[2]);

    yRatio[0]->Divide(yProjections[2]);
    yRatio[1]->Divide(yProjections[2]);
    yRatio[2]->Divide(yProjections[2]);



    // Draw the first X projection to set the axes and labels
    padX->cd();
    xProjections[0]->Draw("L");
    xProjections[0]->GetXaxis()->SetTitle("x [#mum]");
    xProjections[0]->GetYaxis()->SetTitle("Efficiency");
    xProjections[0]->GetYaxis()->SetTitleSize(0.05);
    xProjections[0]->GetYaxis()->SetTitleOffset(0.75);
    xProjections[0]->SetTitle("Projection X");
    for (int i = 0; i < 3; ++i) {
        xProjections[i]->GetYaxis()->SetRangeUser(0.25, 1.0);
        xProjections[i]->Draw("LSAME");
        legendX->AddEntry(xProjections[i], labels[i], "l");
    }
    legendX->Draw();

    // Draw the first Y projection to set the axes and labels
    padY->cd();
    yProjections[0]->Draw("L");
    yProjections[0]->GetXaxis()->SetTitle("y [#mum]");
    yProjections[0]->GetYaxis()->SetTitle("Efficiency");
    yProjections[0]->GetYaxis()->SetTitleSize(0.05);
    yProjections[0]->GetYaxis()->SetTitleOffset(0.75);
    yProjections[0]->SetTitle("Projection Y");
    for (int i = 0; i < 3; ++i) {
        yProjections[i]->GetYaxis()->SetRangeUser(0.25, 1.0);
        yProjections[i]->Draw("LSAME");
        //legendY->AddEntry(yProjections[i], labels[i], "l");
    }
    //legendY->Draw();

    // Draw the X ratio plot
    padXRatio->cd();
    xRatio[0]->Draw("L");
    xRatio[0]->GetXaxis()->SetTitle("x [um]");
    xRatio[0]->GetYaxis()->SetTitle("Ratio to Middle Row");
    xRatio[0]->GetYaxis()->SetTitleSize(0.08);
    xRatio[0]->GetYaxis()->SetTitleOffset(0.4);
    xRatio[0]->GetYaxis()->SetRangeUser(0.7, 1.3);
    xRatio[0]->SetTitle("");

    for (int i = 0; i < 3; ++i) {
        xRatio[i]->Draw("LSAME");
    }

    // Draw the Y ratio plot
    padYRatio->cd();
    yRatio[0]->Draw("L");
    yRatio[0]->GetXaxis()->SetTitle("y [um]");
    yRatio[0]->GetYaxis()->SetTitle("Ratio to Middle Row");
    yRatio[0]->GetYaxis()->SetTitleSize(0.08);
    yRatio[0]->GetYaxis()->SetTitleOffset(0.4);
    yRatio[0]->GetYaxis()->SetRangeUser(0.7, 1.3);
    yRatio[0]->SetTitle("");
    for (int i = 0; i < 3; ++i) {
        yRatio[i]->Draw("LSAME");
    }

    canvas->Update();
}

int main() {
    analysis_outermostPixels_row();
    return 0;
}
