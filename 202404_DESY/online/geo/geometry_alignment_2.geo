[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.01849deg,0.284187deg,0.771087deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.21942mm,-243.513um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.34204deg,-0.83457deg,-0.0539153deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 111.102um,80.888um,24mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.73515deg,-0.712702deg,0.173377deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 410.076um,-351.506um,51mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.171142deg,-0.979586deg,0.137338deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 588.089um,93.195um,149mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.716598deg,-0.544711deg,0.174924deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 585.393um,-56.717um,175mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

