#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TMath.h>
#include <vector>
#include <tuple>

void plotResidualXVsThreshold() {
    // Define vectors to store the data for each file

    std::vector<std::tuple<int, int, double, double>> data0V;
    std::vector<std::tuple<int, int, double, double>> data1V2;
    std::vector<std::tuple<int, int, double, double>> data2V4;
    std::vector<std::tuple<int, int, double, double>> data3V6;



    // Open and read data from run_number_1V2.txt
    std::ifstream inputFile1V2("run_number_1V2.txt");
    if (!inputFile1V2.is_open()) {
        std::cerr << "Failed to open run_number_1V2.txt" << std::endl;
        return;
    }

    std::string line;
    while (std::getline(inputFile1V2, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (1V2): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the residual histogram
        TH1F* residualHist = dynamic_cast<TH1F*>(f->Get("AnalysisDUT/H2M_0/local_residuals/residualsX"));

        if (residualHist) {
            double residual = residualHist->GetRMS();
            double residualError = residual / sqrt(2 * residualHist->GetEntries());

            std::cout << "Residual (1V2): " << residual << std::endl;
            std::cout << "Residual Error (1V2): " << residualError << std::endl;

            // Store the data in the vector
            data1V2.push_back(std::make_tuple(runNumber, threshold, residual, residualError));
        } else {
            std::cerr << "Failed to get residual for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file for 1V2
    inputFile1V2.close();

    // Open and read data from run_number_0V.txt (0V dataset)
    std::ifstream inputFile0V("run_number_0V.txt");
    if (!inputFile0V.is_open()) {
        std::cerr << "Failed to open run_number_0V.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile0V, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (0V): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the residual histogram
        TH1F* residualHist = dynamic_cast<TH1F*>(f->Get("AnalysisDUT/H2M_0/local_residuals/residualsX"));

        if (residualHist) {
            double residual = residualHist->GetRMS();
            double residualError = residual / sqrt(2 * residualHist->GetEntries());

            std::cout << "Residual (0V): " << residual << std::endl;
            std::cout << "Residual Error (0V): " << residualError << std::endl;

            // Store the data in the vector for the 0V file
            data0V.push_back(std::make_tuple(runNumber, threshold, residual, residualError));
        } else {
            std::cerr << "Failed to get residual for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file for 0V
    inputFile0V.close();


    // Open and read data from run_number_2V4.txt
    std::ifstream inputFile2V4("run_number_2V4.txt");
    if (!inputFile2V4.is_open()) {
        std::cerr << "Failed to open run_number_2V4.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile2V4, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (2V4): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the residual histogram
        TH1F* residualHist = dynamic_cast<TH1F*>(f->Get("AnalysisDUT/H2M_0/local_residuals/residualsX"));

        if (residualHist) {
            double residual = residualHist->GetRMS();
            double residualError = residual / sqrt(2 * residualHist->GetEntries());

            std::cout << "Residual (2V4): " << residual << std::endl;
            std::cout << "Residual Error (2V4): " << residualError << std::endl;

            // Store the data in the vector
            data2V4.push_back(std::make_tuple(runNumber, threshold, residual, residualError));
        } else {
            std::cerr << "Failed to get residual for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file for 2V4
    inputFile2V4.close();

    // Open and read data from run_number_3V6.txt
    std::ifstream inputFile3V6("run_number_3V6.txt");
    if (!inputFile3V6.is_open()) {
        std::cerr << "Failed to open run_number_3V6.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile3V6, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (3V6): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the residual histogram
        TH1F* residualHist = dynamic_cast<TH1F*>(f->Get("AnalysisDUT/H2M_0/local_residuals/residualsX"));

        if (residualHist) {
            double residual = residualHist->GetRMS();
            double residualError = residual / sqrt(2 * residualHist->GetEntries());

            std::cout << "Residual (3V6): " << residual << std::endl;
            std::cout << "Residual Error (3V6): " << residualError << std::endl;

            // Store the data in the vector
            data3V6.push_back(std::make_tuple(runNumber, threshold, residual, residualError));
        } else {
            std::cerr << "Failed to get residual for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file for 3V6
    inputFile3V6.close();

    // Create a TCanvas for plotting
    TCanvas* canvas = new TCanvas("canvas", "Residual X vs Threshold", 800, 600);

    // Plot data from run_number_1V2.txt in red
    int nDataPoints1V2 = data1V2.size();
    double thresholds1V2[nDataPoints1V2];
    double residuals1V2[nDataPoints1V2];
    double residualErrors1V2[nDataPoints1V2];

    for (int i = 0; i < nDataPoints1V2; ++i) {
        thresholds1V2[i] = std::get<1>(data1V2[i]); // Threshold
        residuals1V2[i] = std::get<2>(data1V2[i]); // Residual
        residualErrors1V2[i] = std::get<3>(data1V2[i]); // Residual error
    }

    TGraphErrors* graph1V2 = new TGraphErrors(nDataPoints1V2, thresholds1V2, residuals1V2, 0, residualErrors1V2);
    graph1V2->SetTitle("Residual X vs Threshold");
    graph1V2->GetXaxis()->SetTitle("Threshold");
    graph1V2->GetYaxis()->SetTitle("Residual X [#mum]");
    graph1V2->SetMarkerStyle(20);
    graph1V2->SetMarkerColor(kRed);
    graph1V2->SetLineColor(kRed);
    graph1V2->SetLineWidth(2);
    graph1V2->GetXaxis()->SetLabelSize(0.04);  // Adjust label size
    graph1V2->GetYaxis()->SetLabelSize(0.04);
    graph1V2->GetXaxis()->SetTitleSize(0.055);  // Adjust title size
    graph1V2->GetYaxis()->SetTitleSize(0.055);
    graph1V2->GetXaxis()->SetTitleOffset(0.85);  // Adjust title offset
    graph1V2->GetYaxis()->SetTitleOffset(0.85);
    graph1V2->GetXaxis()->SetLabelSize(0.05);  // Adjust axis number size
    graph1V2->GetYaxis()->SetLabelSize(0.05);
    graph1V2->Draw("APL");

    // Plot data from run_number_2V4.txt in blue
    int nDataPoints2V4 = data2V4.size();
    double thresholds2V4[nDataPoints2V4];
    double residuals2V4[nDataPoints2V4];
    double residualErrors2V4[nDataPoints2V4];

    for (int i = 0; i < nDataPoints2V4; ++i) {
        thresholds2V4[i] = std::get<1>(data2V4[i]); // Threshold
        residuals2V4[i] = std::get<2>(data2V4[i]); // Residual
        residualErrors2V4[i] = std::get<3>(data2V4[i]); // Residual error
    }

    TGraphErrors* graph2V4 = new TGraphErrors(nDataPoints2V4, thresholds2V4, residuals2V4, 0, residualErrors2V4);
    graph2V4->SetMarkerStyle(21);
    graph2V4->SetMarkerColor(kBlue);
    graph2V4->SetLineColor(kBlue);
    graph2V4->SetLineWidth(2);
    graph2V4->Draw("PL");

    // Plot data from run_number_3V6.txt in green
    int nDataPoints3V6 = data3V6.size();
    double thresholds3V6[nDataPoints3V6];
    double residuals3V6[nDataPoints3V6];
    double residualErrors3V6[nDataPoints3V6];

    for (int i = 0; i < nDataPoints3V6; ++i) {
        thresholds3V6[i] = std::get<1>(data3V6[i]); // Threshold
        residuals3V6[i] = std::get<2>(data3V6[i]); // Residual
        residualErrors3V6[i] = std::get<3>(data3V6[i]); // Residual error
    }

    TGraphErrors* graph3V6 = new TGraphErrors(nDataPoints3V6, thresholds3V6, residuals3V6, 0, residualErrors3V6);
    graph3V6->SetMarkerStyle(22);
    graph3V6->SetMarkerColor(kGreen);
    graph3V6->SetLineColor(kGreen);
    graph3V6->SetLineWidth(2);
    graph3V6->Draw("PL");

    // Plot data from run_number_0V.txt (0V) in orange
    int nDataPoints0V = data0V.size();
    double thresholds0V[nDataPoints0V];
    double residuals0V[nDataPoints0V];
    double residualErrors0V[nDataPoints0V];

    for (int i = 0; i < nDataPoints0V; ++i) {
        thresholds0V[i] = std::get<1>(data0V[i]); // Threshold
        residuals0V[i] = std::get<2>(data0V[i]); // Residual
        residualErrors0V[i] = std::get<3>(data0V[i]); // Residual error
    }

    TGraphErrors* graph0V = new TGraphErrors(nDataPoints0V, thresholds0V, residuals0V, 0, residualErrors0V);
    graph0V->SetMarkerStyle(23);  // Change marker style to 23 (for example)
    graph0V->SetMarkerColor(kOrange);  // Change marker color to orange
    graph0V->SetLineColor(kOrange);  // Change line color to orange
    graph0V->SetLineWidth(2);  // Change line width
    graph0V->Draw("PL");


    // Create a legend with all entries
    TLegend* legend = new TLegend(0.6, 0.6, 0.85, 0.85);
    legend->AddEntry(graph3V6, "-3.6 V", "lp");
    legend->AddEntry(graph2V4, "-2.4 V", "lp");
    legend->AddEntry(graph1V2, "-1.2 V", "lp");
    legend->AddEntry(graph0V, "0 V", "lp");
    legend->SetBorderSize(0);
    legend->Draw();

    //canvas->SaveAs("residual_X_vs_threshold.pdf"); // If you want to save the plot
}

int main() {
    plotResidualXVsThreshold();
    return 0;
}
