[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.0526548deg,0.0221162deg,0.797385deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.37712mm,62.059um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.753783deg,-0.396315deg,-0.0635983deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 190.674um,435.758um,105mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.96547deg,-0.405138deg,0.167246deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 604.453um,39.232um,132mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = -0.191769deg,0.0735678deg,0.233996deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -768.47um,194.576um,172mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,194mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.441349deg,-0.540987deg,0.143641deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 719.856um,90.3um,221mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.751491deg,-1.53404deg,0.157048deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.40128mm,-72.118um,325mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

