#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TH1.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TMath.h>
#include <TLegend.h>

void plotClusterSizeYVsThreshold() {
    // Define a vector to store the data: runNumber, threshold, cluster size, cluster size error
    std::vector<std::tuple<int, int, double, double>> data0V;
    std::vector<std::tuple<int, int, double, double>> data1V2;
    std::vector<std::tuple<int, int, double, double>> data2V4;
    std::vector<std::tuple<int, int, double, double>> data3V6;


    // Open and read data from run_number_1V2.txt
    std::ifstream inputFile1V2("run_number_1V2.txt");
    if (!inputFile1V2.is_open()) {
        std::cerr << "Failed to open run_number_1V2.txt" << std::endl;
        return;
    }

    std::string line;
    while (std::getline(inputFile1V2, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (1V2): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the cluster size histogram
        TH1F* clusterSizeHist = dynamic_cast<TH1F*>(f->Get("ClusteringSpatial/H2M_0/clusterWidthRow"));

        if (clusterSizeHist) {
            double clusterSize = clusterSizeHist->GetMean();
            double clusterSizeError = clusterSizeHist->GetRMS() / sqrt(clusterSizeHist->GetEntries());

            std::cout << "Cluster Size: " << clusterSize << std::endl;
            std::cout << "Cluster Size Error: " << clusterSizeError << std::endl;

            // Store the data in the vector
            data1V2.push_back(std::make_tuple(runNumber, threshold, clusterSize, clusterSizeError));
        } else {
            std::cerr << "Failed to get cluster size for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile1V2.close();

    // Open and read data from run_number_0V.txt (New section)
    std::ifstream inputFile0V("run_number_0V.txt");
    if (!inputFile0V.is_open()) {
        std::cerr << "Failed to open run_number_0V.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile0V, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (0V): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file for 0V
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file for 0V
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the cluster size histogram for 0V
        TH1F* clusterSizeHist = dynamic_cast<TH1F*>(f->Get("ClusteringSpatial/H2M_0/clusterWidthRow"));

        if (clusterSizeHist) {
            double clusterSize = clusterSizeHist->GetMean();
            double clusterSizeError = clusterSizeHist->GetRMS() / sqrt(clusterSizeHist->GetEntries());

            std::cout << "Cluster Size (0V): " << clusterSize << std::endl;
            std::cout << "Cluster Size Error (0V): " << clusterSizeError << std::endl;

            // Store the data in the vector for 0V
            data0V.push_back(std::make_tuple(runNumber, threshold, clusterSize, clusterSizeError));
        } else {
            std::cerr << "Failed to get cluster size for Run " << runNumber << std::endl;
        }

        // Close the ROOT file for 0V
        f->Close();
    }

        // Close the input file for 0V
        inputFile0V.close();


    std::ifstream inputFile2V4("run_number_2V4.txt");
    if (!inputFile2V4.is_open()) {
        std::cerr << "Failed to open run_number_2V4.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile2V4, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (2V4): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the cluster size histogram
        TH1F* clusterSizeHist = dynamic_cast<TH1F*>(f->Get("ClusteringSpatial/H2M_0/clusterWidthRow"));

        if (clusterSizeHist) {
            double clusterSize = clusterSizeHist->GetMean();
            double clusterSizeError = clusterSizeHist->GetRMS() / sqrt(clusterSizeHist->GetEntries());

            std::cout << "Cluster Size: " << clusterSize << std::endl;
            std::cout << "Cluster Size Error: " << clusterSizeError << std::endl;

            // Store the data in the vector for 2V4
            data2V4.push_back(std::make_tuple(runNumber, threshold, clusterSize, clusterSizeError));
        } else {
            std::cerr << "Failed to get cluster size for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile2V4.close();

    // Open and read data from run_number_3V6.txt
    std::ifstream inputFile3V6("run_number_3V6.txt");
    if (!inputFile3V6.is_open()) {
        std::cerr << "Failed to open run_number_3V6.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile3V6, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (3V6): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the cluster size histogram
        TH1F* clusterSizeHist = dynamic_cast<TH1F*>(f->Get("ClusteringSpatial/H2M_0/clusterWidthRow"));

        if (clusterSizeHist) {
            double clusterSize = clusterSizeHist->GetMean();
            double clusterSizeError = clusterSizeHist->GetRMS() / sqrt(clusterSizeHist->GetEntries());

            std::cout << "Cluster Size: " << clusterSize << std::endl;
            std::cout << "Cluster Size Error: " << clusterSizeError << std::endl;

            // Store the data in the vector for 3V6
            data3V6.push_back(std::make_tuple(runNumber, threshold, clusterSize, clusterSizeError));
        } else {
            std::cerr << "Failed to get cluster size for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile3V6.close();

    // Create a TCanvas for plotting
    TCanvas* canvas = new TCanvas("canvas", "Cluster Size Y vs Threshold", 800, 600);

    // Plot data from run_number_1V2.txt in red
    int nDataPoints1V2 = data1V2.size();
    double thresholds1V2[nDataPoints1V2];
    double clusterSizes1V2[nDataPoints1V2];
    double clusterSizeErrors1V2[nDataPoints1V2];

    for (int i = 0; i < nDataPoints1V2; ++i) {
        thresholds1V2[i] = std::get<1>(data1V2[i]); // Threshold
        clusterSizes1V2[i] = std::get<2>(data1V2[i]); // Cluster size
        clusterSizeErrors1V2[i] = std::get<3>(data1V2[i]); // Cluster size error
    }

    TGraphErrors* graph1V2 = new TGraphErrors(nDataPoints1V2, thresholds1V2, clusterSizes1V2, 0, clusterSizeErrors1V2);
    graph1V2->SetTitle("Cluster Size Y vs Threshold");
    graph1V2->GetXaxis()->SetTitle("Threshold");
    graph1V2->GetYaxis()->SetTitle("Cluster Size Y");
    graph1V2->SetMarkerStyle(20);
    graph1V2->SetMarkerColor(kRed);
    graph1V2->SetLineColor(kRed);
    graph1V2->SetLineWidth(2);
    graph1V2->Draw("APL");

    // Plot data from run_number_2V4.txt in blue
    int nDataPoints2V4 = data2V4.size();
    double thresholds2V4[nDataPoints2V4];
    double clusterSizes2V4[nDataPoints2V4];
    double clusterSizeErrors2V4[nDataPoints2V4];

    for (int i = 0; i < nDataPoints2V4; ++i) {
        thresholds2V4[i] = std::get<1>(data2V4[i]); // Threshold
        clusterSizes2V4[i] = std::get<2>(data2V4[i]); // Cluster size
        clusterSizeErrors2V4[i] = std::get<3>(data2V4[i]); // Cluster size error
    }

    TGraphErrors* graph2V4 = new TGraphErrors(nDataPoints2V4, thresholds2V4, clusterSizes2V4, 0, clusterSizeErrors2V4);
    graph2V4->SetTitle("Cluster Size Y vs Threshold");
    graph2V4->GetXaxis()->SetTitle("Threshold");
    graph2V4->GetYaxis()->SetTitle("Cluster Size Y");
    graph2V4->SetMarkerStyle(21); // Change marker style for 2V4
    graph2V4->SetMarkerColor(kBlue); // Change marker color for 2V4
    graph2V4->SetLineColor(kBlue); // Change line color for 2V4
    graph2V4->SetLineWidth(2);
    graph2V4->Draw("PL");

    int nDataPoints0V = data0V.size();
    double thresholds0V[nDataPoints0V];
    double clusterSizes0V[nDataPoints0V];
    double clusterSizeErrors0V[nDataPoints0V];

    for (int i = 0; i < nDataPoints0V; ++i) {
        thresholds0V[i] = std::get<1>(data0V[i]); // Threshold
        clusterSizes0V[i] = std::get<2>(data0V[i]); // Cluster size
        clusterSizeErrors0V[i] = std::get<3>(data0V[i]); // Cluster size error
    }

    TGraphErrors* graph0V = new TGraphErrors(nDataPoints0V, thresholds0V, clusterSizes0V, 0, clusterSizeErrors0V);
    graph0V->SetTitle("Cluster Size Y vs Threshold");
    graph0V->GetXaxis()->SetTitle("Threshold");
    graph0V->GetYaxis()->SetTitle("Cluster Size Y");
    graph0V->SetMarkerStyle(23); // Set marker style for 0V
    graph0V->SetMarkerColor(kOrange); // Set marker color for 0V
    graph0V->SetLineColor(kOrange); // Set line color for 0V
    graph0V->SetLineWidth(2);
    graph0V->Draw("PL");



    // Plot data from run_number_3V6.txt in green
    int nDataPoints3V6 = data3V6.size();
    double thresholds3V6[nDataPoints3V6];
    double clusterSizes3V6[nDataPoints3V6];
    double clusterSizeErrors3V6[nDataPoints3V6];

    for (int i = 0; i < nDataPoints3V6; ++i) {
        thresholds3V6[i] = std::get<1>(data3V6[i]); // Threshold
        clusterSizes3V6[i] = std::get<2>(data3V6[i]); // Cluster size
        clusterSizeErrors3V6[i] = std::get<3>(data3V6[i]); // Cluster size error
    }

    TGraphErrors* graph3V6 = new TGraphErrors(nDataPoints3V6, thresholds3V6, clusterSizes3V6, 0, clusterSizeErrors3V6);
    graph3V6->SetMarkerStyle(22); // Change marker style for 3V6
    graph3V6->SetMarkerColor(kGreen); // Change marker color for 3V6
    graph3V6->SetLineColor(kGreen); // Change line color for 3V6
    graph3V6->SetLineWidth(2);
    graph3V6->Draw("PL");

    // Create a legend with all entries
    TLegend* legend = new TLegend(0.6, 0.6, 0.85, 0.85);
    legend->AddEntry(graph3V6, "-3.6 V", "lp");
    legend->AddEntry(graph2V4, "-2.4 V", "lp");
    legend->AddEntry(graph1V2, "-1.2 V", "lp");
    legend->AddEntry(graph0V, "0 V", "lp");
    legend->SetBorderSize(0);
    legend->Draw();
}

int main() {
    plotClusterSizeYVsThreshold();
    return 0;
}
