#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TEfficiency.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TGraph.h>
#include <TLegend.h>

void plotEfficiencyVsThreshold() {
    // Define vectors to store the data for each file
    std::vector<std::tuple<int, int, double, double, double>> data0V;
    std::vector<std::tuple<int, int, double, double, double>> data1V2;
    std::vector<std::tuple<int, int, double, double, double>> data2V4;
    std::vector<std::tuple<int, int, double, double, double>> data3V6;

    // Open and read data from run_number_1V2.txt
    std::ifstream inputFile1V2("run_number_1V2.txt");
    if (!inputFile1V2.is_open()) {
        std::cerr << "Failed to open run_number_1V2.txt" << std::endl;
        return;
    }

    std::string line;
    while (std::getline(inputFile1V2, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (1V2): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("histograms_%d_online.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the efficiency value, and its errors
        TEfficiency* efficiencyHist = (TEfficiency*)gFile->Get("AnalysisEfficiency/H2M_0/eTotalEfficiency");

        if (efficiencyHist) {
            double efficiency = efficiencyHist->GetEfficiency(1);
            double efficiencyErrorLow = efficiencyHist->GetEfficiencyErrorLow(1);
            double efficiencyErrorUp = efficiencyHist->GetEfficiencyErrorUp(1);

            std::cout << "Efficiency (1V2): " << efficiency << std::endl;
            std::cout << "Efficiency Error Low (1V2): " << efficiencyErrorLow << std::endl;
            std::cout << "Efficiency Error Up (1V2): " << efficiencyErrorUp << std::endl;

            // Store the data in the vector
            data1V2.push_back(std::make_tuple(runNumber, threshold, efficiency, efficiencyErrorLow, efficiencyErrorUp));
        } else {
            std::cerr << "Failed to get efficiency for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile1V2.close();




    // Create a TCanvas for plotting
    TCanvas* canvas = new TCanvas("canvas", "Efficiency vs Threshold", 800, 600);

    // Plot data from run_number_1V2.txt in red
    int nDataPoints1V2 = data1V2.size();
    double thresholds1V2[nDataPoints1V2];
    double efficiencies1V2[nDataPoints1V2];
    double efficiencyErrorLow1V2[nDataPoints1V2];
    double efficiencyErrorUp1V2[nDataPoints1V2];

    for (int i = 0; i < nDataPoints1V2; ++i) {
        thresholds1V2[i] = std::get<1>(data1V2[i]); // Threshold
        efficiencies1V2[i] = std::get<2>(data1V2[i]); // Efficiency
        efficiencyErrorLow1V2[i] = std::get<3>(data1V2[i]); // Efficiency error low
        efficiencyErrorUp1V2[i] = std::get<4>(data1V2[i]); // Efficiency error up
    }

    TGraphAsymmErrors* graph1V2 = new TGraphAsymmErrors(nDataPoints1V2, thresholds1V2, efficiencies1V2, 0, 0, efficiencyErrorLow1V2, efficiencyErrorUp1V2);
    graph1V2->SetTitle("Efficiency vs Threshold");
    graph1V2->GetXaxis()->SetTitle("Threshold");
    graph1V2->GetYaxis()->SetTitle("Efficiency");
    graph1V2->SetMarkerStyle(20);
    graph1V2->SetMarkerColor(kRed);
    graph1V2->SetLineColor(kRed);
    graph1V2->SetLineWidth(2);
    graph1V2->GetXaxis()->SetLabelSize(0.04);  // Adjust label size
    graph1V2->GetYaxis()->SetLabelSize(0.04);
    graph1V2->GetXaxis()->SetTitleSize(0.055);  // Adjust title size
    graph1V2->GetYaxis()->SetTitleSize(0.055);
    graph1V2->GetXaxis()->SetTitleOffset(0.85);  // Adjust title offset
    graph1V2->GetYaxis()->SetTitleOffset(0.85);
    graph1V2->GetXaxis()->SetLabelSize(0.05);  // Adjust axis number size
    graph1V2->GetYaxis()->SetLabelSize(0.05);
    graph1V2->Draw("APL");

    // Plot data from run_number_0V.txt in black
    int nDataPoints0V = data0V.size();
    double thresholds0V[nDataPoints0V];
    double efficiencies0V[nDataPoints0V];
    double efficiencyErrorLow0V[nDataPoints0V];
    double efficiencyErrorUp0V[nDataPoints0V];

    for (int i = 0; i < nDataPoints0V; ++i) {
        thresholds0V[i] = std::get<1>(data0V[i]); // Threshold
        efficiencies0V[i] = std::get<2>(data0V[i]); // Efficiency
        efficiencyErrorLow0V[i] = std::get<3>(data0V[i]); // Efficiency error low
        efficiencyErrorUp0V[i] = std::get<4>(data0V[i]); // Efficiency error up
    }

    TGraphAsymmErrors* graph0V = new TGraphAsymmErrors(nDataPoints0V, thresholds0V, efficiencies0V, 0, 0, efficiencyErrorLow0V, efficiencyErrorUp0V);
    graph0V->SetMarkerStyle(23); // Choose the marker style you prefer
    graph0V->SetMarkerColor(kOrange); // Set the marker color
    graph0V->SetLineColor(kOrange); // Set the line color
    graph0V->SetLineWidth(2);
    graph0V->Draw("PL");


    // Plot data from run_number_2V4.txt in blue
    int nDataPoints2V4 = data2V4.size();
    double thresholds2V4[nDataPoints2V4];
    double efficiencies2V4[nDataPoints2V4];
    double efficiencyErrorLow2V4[nDataPoints2V4];
    double efficiencyErrorUp2V4[nDataPoints2V4];

    for (int i = 0; i < nDataPoints2V4; ++i) {
        thresholds2V4[i] = std::get<1>(data2V4[i]); // Threshold
        efficiencies2V4[i] = std::get<2>(data2V4[i]); // Efficiency
        efficiencyErrorLow2V4[i] = std::get<3>(data2V4[i]); // Efficiency error low
        efficiencyErrorUp2V4[i] = std::get<4>(data2V4[i]); // Efficiency error up
    }

    TGraphAsymmErrors* graph2V4 = new TGraphAsymmErrors(nDataPoints2V4, thresholds2V4, efficiencies2V4, 0, 0, efficiencyErrorLow2V4, efficiencyErrorUp2V4);
    graph2V4->SetMarkerStyle(21);
    graph2V4->SetMarkerColor(kBlue);
    graph2V4->SetLineColor(kBlue);
    graph2V4->SetLineWidth(2);
    graph2V4->Draw("PL");

    int nDataPoints3V6 = data3V6.size();
    double thresholds3V6[nDataPoints3V6];
    double efficiencies3V6[nDataPoints3V6];
    double efficiencyErrorLow3V6[nDataPoints3V6];
    double efficiencyErrorUp3V6[nDataPoints3V6];

    for (int i = 0; i < nDataPoints3V6; ++i) {
        thresholds3V6[i] = std::get<1>(data3V6[i]); // Threshold
        efficiencies3V6[i] = std::get<2>(data3V6[i]); // Efficiency
        efficiencyErrorLow3V6[i] = std::get<3>(data3V6[i]); // Efficiency error low
        efficiencyErrorUp3V6[i] = std::get<4>(data3V6[i]); // Efficiency error up
    }

    TGraphAsymmErrors* graph3V6 = new TGraphAsymmErrors(nDataPoints3V6, thresholds3V6, efficiencies3V6, 0, 0, efficiencyErrorLow3V6, efficiencyErrorUp3V6);
    graph3V6->SetMarkerStyle(22);
    graph3V6->SetMarkerColor(kGreen);
    graph3V6->SetLineColor(kGreen);
    graph3V6->SetLineWidth(2);
    graph3V6->Draw("PL");

    // Create a legend with all entries
    TLegend* legend = new TLegend(0.6, 0.6, 0.85, 0.85);
    legend->AddEntry(graph3V6, "-3.6 V", "lp");
    legend->AddEntry(graph2V4, "-2.4 V", "lp");
    legend->AddEntry(graph1V2, "-1.2 V", "lp");
    legend->AddEntry(graph0V, "0 V", "lp");
    legend->SetBorderSize(0);
    legend->Draw();

    //canvas->SaveAs("efficiency_vs_threshold.pdf"); // If you want to save the plot
}

int main() {
    plotEfficiencyVsThreshold();
    return 0;
}
