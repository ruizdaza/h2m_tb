[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.13703deg,0.503515deg,0.771602deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.21923mm,-243.415um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.37384deg,-0.996889deg,-0.0530559deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 111.024um,80.906um,24mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.77382deg,-0.749314deg,0.173778deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 410.037um,-351.468um,51mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.794692deg,-0.139458deg,0.137223deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 588.157um,93.319um,149mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.859437deg,0.275249deg,0.177044deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 585.13um,-56.636um,175mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

