[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.70358deg,0.832851deg,0.771373deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.03108mm,-303.785um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.75503deg,1.17439deg,-0.0504203deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -103.377um,12.14um,24mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -2.02764deg,-0.792286deg,0.177732deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 169.886um,-428.927um,51mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = -0.160027deg,-0.0263561deg,0.0907565deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -532.845um,1.14178mm,96mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.36845deg,-0.538294deg,0.141463deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 609.009um,90.244um,149mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.57655deg,0.688982deg,0.184893deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 626.447um,-63.091um,175mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

