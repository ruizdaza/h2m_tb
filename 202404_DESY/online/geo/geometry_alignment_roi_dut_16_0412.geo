[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.60961deg,0.608596deg,0.773378deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.33604mm,-211.479um,0um
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.62456deg,1.02777deg,-0.0494463deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 142.026um,86.823um,24mm
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.91041deg,-0.861786deg,0.176643deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 353.488um,-372.083um,51mm
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.0379871deg,0.0357526deg,-0.822137deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -730.222um,1.23861mm,96mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
roi = [[350,250],[350,380],[580,380],[580,250]]
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.32256deg,-0.68182deg,0.139974deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 566.768um,82.091um,149mm
roi = [[350,250],[350,380],[580,380],[580,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.60617deg,0.577484deg,0.184951deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 515.563um,-92.946um,175mm
roi = [[350,250],[350,380],[580,380],[580,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

