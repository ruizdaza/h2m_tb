[W0013_D04]
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.614deg,-189.201deg,-0.493317deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 548.814um,236.365um,0um
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_E03]
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189.033deg,-189.274deg,-0.47418deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -149.3um,167.756um,21mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_G02]
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189deg,-189deg,0deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0um,0um,42mm
role = "reference"
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

#[W0068_I11]
#number_of_pixels = 256, 256
#orientation = 180.024deg,0.00498473deg,90.1572deg
#orientation_mode = "xyz"
#pixel_pitch = 55um,55um
#position = 0um,0um,113mm
#role = "dut"
#spatial_resolution = 2.7um,2.7um
#time_resolution = 0.451ns
#type = "timepix3"

[W0013_G03]
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.614deg,-8.39945deg,-0.954605deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 60.459um,-191.525um,185.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_J05]
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.826deg,-8.47542deg,-0.810105deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -83.953um,-72.563um,207.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_L09]
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.533deg,-8.41566deg,-0.510849deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -131.09um,-140.896um,229mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

