[W0013_D04]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 186.02deg,-189.343deg,-0.54557deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 555.278um,223.349um,0um
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_E03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189.444deg,-188.916deg,-0.50426deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -146.41um,162.45um,21mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_G02]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189deg,-189deg,0deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0um,0um,42.5mm
role = "reference"
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[H2M_0]
coordinates = "cartesian"
number_of_pixels = 64, 16
orientation = 179.969deg,0.0191368deg,0.760773deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 447.787um,36.65um,113mm
role = "dut"
spatial_resolution = 10um,10um
time_resolution = 3ns
type = "h2m"

[W0013_G03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.559deg,-8.47324deg,-0.899143deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -83.775um,-148.633um,185.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_J05]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.57deg,-8.54664deg,-0.825403deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -137.221um,-5.182um,207.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_L09]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.234deg,-8.48562deg,-0.526491deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -187.432um,-65.534um,229mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

