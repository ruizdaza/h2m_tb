[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.0160428deg,-0.0364401deg,0.812855deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.60241mm,101.906um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.35214deg,-0.00223454deg,-0.0734532deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 402.944um,500.133um,108mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -2.13054deg,-0.241788deg,0.151089deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 813.539um,109.765um,135mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = -0.0923608deg,-0.0504203deg,-0.579604deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -716.394um,9.329um,175mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,202mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.0094538deg,-0.0433729deg,0.0499619deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 716.211um,94.934um,230mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.0552904deg,-6.1507deg,0.0614211deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.42979mm,-57.33um,335mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

