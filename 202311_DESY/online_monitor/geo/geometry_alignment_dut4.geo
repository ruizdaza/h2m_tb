[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.0682393deg,0.103132deg,0.789708deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.37656mm,60.38um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.552102deg,-0.531017deg,-0.0648015deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 190.667um,435.727um,105mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.89958deg,-0.421582deg,0.167533deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 604.622um,39.898um,132mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
mask_file = "/home/teleuser/h2m/tb_desy_202311/masking.txt"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.0128343deg,0.00727656deg,1.14804deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -705.132um,225.334um,172mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,194mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.687664deg,-0.412014deg,0.143984deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 719.851um,90.258um,221mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.658271deg,-1.65774deg,0.15487deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.40157mm,-70.715um,325mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

