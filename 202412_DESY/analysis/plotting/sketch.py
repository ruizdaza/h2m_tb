import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import ScalarFormatter

with np.load("ballistic_deficit.npz") as data:
    A10x = data["A10x"]
    A10y = data["A10y"]
    A21x = data["A21x"]
    A21y = data["A21y"]
    B10x = data["B10x"]
    B10y = data["B10y"]
    B21x = data["B21x"]
    B21y = data["B21y"]

# Downsample each dataset by taking every 10th point
A10x_reduced = A10x[::100]
A10y_reduced = A10y[::100]
A21x_reduced = A21x[::100]
A21y_reduced = A21y[::100]
B10x_reduced = B10x[::100]
B10y_reduced = B10y[::100]
B21x_reduced = B21x[::100]
B21y_reduced = B21y[::100]

fig, ax1 = plt.subplots()

# Plot the downsampled data
ax1.plot(A21x_reduced * 1e9, A21y_reduced, 'g', label="fast signal, ikrum high")
ax1.plot(A10x_reduced * 1e9, A10y_reduced, 'g--', label="fast signal, ikrum low")
ax1.plot(B10x_reduced * 1e9, B10y_reduced, 'r', label="slow signal, ikrum high")
ax1.plot(B21x_reduced * 1e9, B21y_reduced, 'r--', label="slow signal, ikrum low")

# Remove the rectangular frame by hiding the spines
for spine in ax1.spines.values():
    spine.set_visible(False)

# Remove tick numbers
ax1.set_xticks([])
ax1.set_yticks([])

# Add axis arrows
ax1.annotate("", xy=(1, 0), xytext=(0, 0),
             arrowprops=dict(arrowstyle="->", color="black", lw=1.5),
             xycoords="axes fraction")
ax1.annotate("", xy=(0, 1), xytext=(0, 0),
             arrowprops=dict(arrowstyle="->", color="black", lw=1.5),
             xycoords="axes fraction")

# Keep axis labels at original positions
ax1.set_xlabel("Time [ns]", fontsize=16, labelpad=10)
ax1.set_ylabel("CSA Output [a.u]", fontsize=16, labelpad=10)

# Customize legend
ax1.legend(loc="lower center", frameon=False, fontsize=12)

# Set plot limits
plt.xlim([0, 500])
plt.ylim([0.30, 0.5])

plt.show()



plt.savefig("ballistic_deficit_sketch.pdf")
