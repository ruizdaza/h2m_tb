#!/usr/bin/bash
datapath=/scratch/ruizdaza/h2m/h2m_tb/202311_DESY/data
#corryconfig=dut_alignment_with_telepix.conf
#corryconfig=alignment_with_telepix.conf
#corryconfig=prealignment_with_telepix.conf
corryconfig=analysis_with_telepix.conf

run=${1}
#latest_telescope=$(find $datapath -name "*tel*" -print0 | xargs -r -0 ls -1 -t | head -n1)
latest_telescope=$(find $datapath -name "*${run}_tel*" -print0 | xargs -r -0 ls -1 -t | head -n1)
latest_h2m=$(find $datapath -name "*${run}_h2m*" -print0 | xargs -r -0 ls -1 -t | head -n1)
latest_telepix=$(find $datapath -name "*single*${run}*" -print0 | xargs -r -0 ls -1 -t | head -n1)

telescope_run=$(echo $latest_telescope | grep -o "run[0-9]*")
h2m_run=$(echo $latest_h2m | grep -o "run[0-9]*")
telepix_run=$(echo $latest_telepix | grep -o "run[0-9]*")

#if [[ $telescope_run != $apts_run ]]; then
#    echo "$latest_telescope and $latest_h2m seem to be from different runs" ;
#else
    echo "*********************************************"
    echo "* opening $latest_telescope , $latest_h2m and $latest_telepix" 
    echo "*********************************************"

/scratch/ruizdaza/corryvreckan/bin/corry -c ${corryconfig} \
                   -o histogram_file=histograms_${run}_withTelepix.root \
                   -o EventLoaderEUDAQ2:TLU_0.file_name=${latest_telescope} \
                   -o EventLoaderEUDAQ2:H2M_0.file_name=${latest_h2m} \
                   -o EventLoaderEUDAQ2.file_name=${latest_telescope} 
#                   -o EventLoaderMuPixTelescope.input_file=${latest_telepix}




