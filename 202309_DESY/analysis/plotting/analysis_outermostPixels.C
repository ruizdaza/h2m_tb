#include <iostream>
#include <TFile.h>
#include <TProfile2D.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TAxis.h>

void analysis_outermostPixels() {
    // Create a single canvas for X and Y projections
    TCanvas* canvas = new TCanvas("ProjectionCanvas", "Projections", 1200, 600);
    TPad* padX = new TPad("padX", "X Projection", 0.0, 0.0, 0.5, 1.0);
    TPad* padY = new TPad("padY", "Y Projection", 0.5, 0.0, 1.0, 1.0);
    padX->Draw();
    padY->Draw();

    // Create legends for X and Y projections
    TLegend* legendX = new TLegend(0.6, 0.7, 0.9, 0.9);
    TLegend* legendY = new TLegend(0.6, 0.7, 0.9, 0.9);

    const char* fileNames[5] = {
        "/scratch/ruizdaza/h2m/h2m_tb/202309_DESY/analysis/dut_analysis_topRow.root",
        "/scratch/ruizdaza/h2m/h2m_tb/202309_DESY/analysis/dut_analysis_bottomRow.root",
        "/scratch/ruizdaza/h2m/h2m_tb/202309_DESY/analysis/dut_analysis_leftCol.root",
        "/scratch/ruizdaza/h2m/h2m_tb/202309_DESY/analysis/dut_analysis_rightCol.root",
        "/scratch/ruizdaza/h2m/h2m_tb/202309_DESY/analysis/output_469/dut_analysis.root"
    };

    TH1D* xProjections[5];
    TH1D* yProjections[5];
    const char* labels[5] = {"Top Row", "Bottom Row", "Left Column", "Right Column", "Full Matrix"};
    int colors[5] = {2, 3, 4, 6, 1}; // Line colors (1 = black)

    for (int i = 0; i < 5; ++i) {
        TFile* file = TFile::Open(fileNames[i]);
        if (!file || file->IsZombie()) {
            std::cerr << "Error: Cannot open the ROOT file " << fileNames[i] << std::endl;
            continue;  // Move to the next file if one fails
        }

        TProfile2D* profile2D = dynamic_cast<TProfile2D*>(file->Get("AnalysisEfficiency/H2M_0/pixelEfficiencyMap_trackPos_TProfile"));
        if (!profile2D) {
            std::cerr << "Error: Cannot retrieve the TProfile2D from " << fileNames[i] << std::endl;
            file->Close();
            continue;  // Move to the next file if one fails
        }

        TH2D* hist2D = profile2D->ProjectionXY();
        xProjections[i] = hist2D->ProjectionX();
        yProjections[i] = hist2D->ProjectionY();

        xProjections[i]->SetLineColor(colors[i]);  // Set line colors
        xProjections[i]->SetStats(0);  // Disable statistics box
        xProjections[i]->SetLineWidth(2);  // Set line width
        xProjections[i]->Scale(1.0 / 35.0);  // Divide by 35

        yProjections[i]->SetLineColor(colors[i]);  // Set line colors
        yProjections[i]->SetStats(0);  // Disable statistics box
        yProjections[i]->SetLineWidth(2);  // Set line width
        yProjections[i]->Scale(1.0 / 35.0);  // Divide by 35
    }

    // Draw the first X projection to set the axes and labels
    padX->cd();
    xProjections[0]->Draw("L");
    xProjections[0]->GetXaxis()->SetTitle("x [um]");
    xProjections[0]->GetYaxis()->SetTitle("Efficiency");
    xProjections[0]->SetTitle("Projection X");
    for (int i = 0; i < 5; ++i) {
        xProjections[i]->GetYaxis()->SetRangeUser(0.25, 1.0);
        xProjections[i]->Draw("LSAME");
        legendX->AddEntry(xProjections[i], labels[i], "l");
    }
    legendX->Draw();

    // Draw the first Y projection to set the axes and labels
    padY->cd();
    yProjections[0]->Draw("L");
    yProjections[0]->GetXaxis()->SetTitle("y [um]");
    yProjections[0]->GetYaxis()->SetTitle("Efficiency");
    yProjections[0]->SetTitle("Projection Y");
    for (int i = 0; i < 5; ++i) {
        yProjections[i]->GetYaxis()->SetRangeUser(0.25, 1.0);
        yProjections[i]->Draw("LSAME");
        legendY->AddEntry(yProjections[i], labels[i], "l");
    }
    legendY->Draw();

    canvas->Update();
}

int main() {
    analysis_outermostPixels();
    return 0;
}
