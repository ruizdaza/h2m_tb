[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.130176deg,0.0126051deg,0.799391deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.37705mm,61.615um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.16207deg,-0.58436deg,-0.0614211deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 190.745um,435.432um,105mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -2.2013deg,-0.563848deg,0.170168deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 604.521um,39.269um,132mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
mask_file = "/scratch/ruizdaza/h2m/h2m_tb/202311_DESY/analysis_with_telepix/masking.txt"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.0121467deg,0.0729948deg,0.256914deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -767.762um,193.906um,172mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,194mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.33568deg,0.25869deg,0.139458deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 719.695um,89.514um,221mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -2.30117deg,1.30187deg,0.135447deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.40142mm,-73.514um,325mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[mp10_0]
coordinates = "cartesian"
material_budget = 0.00375
number_of_pixels = 120, 400
orientation = 179.787deg,2.36666deg,-0.673455deg
orientation_mode = "xyz"
pixel_pitch = 165um,25um
position = 2.39811mm,1.00742mm,386mm
spatial_resolution = 43um,14um
time_offset = -216ns
time_resolution = 200ns
type = "telepix2"

