[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.963314deg,180.817deg,-0.65151deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.25567mm,78.346um,8mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.02668deg,180.206deg,0.143239deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -205.324um,516.449um,156mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.75537deg,179.894deg,-0.10445deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -577.703um,128.691um,183mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
mask_file = "/scratch/ruizdaza/h2m/h2m_tb/202405_DESY/analysis_toa/geo/masking_1V2.txt"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 180.252deg,-0.928822deg,89.5637deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -6.04601mm,1.41359mm,222mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,246mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.384512deg,179.188deg,-0.150745deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -643.474um,76.932um,273mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.52672deg,181.592deg,-0.224829deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -1.07287mm,-94.348um,423mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

