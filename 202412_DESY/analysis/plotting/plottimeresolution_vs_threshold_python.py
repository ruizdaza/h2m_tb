import ROOT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker

def plot_timeresolution_vs_threshold_compare_bias():
    baseline = 97.6
    calibration_factor = 32
    calibration_error = 1

    # Initialize data storage for each bias voltage
    data_spatialresolutionX_1V2, data_spatialresolutionX_3V6, data_spatialresolutionX_2V4 = [], [], []
    data_single_point = []
    data_single_point_afteretacorrection = []
    data_single_point_thr109 = []
    data_single_point_aftercal_thr109 = []

    # Function to read spatialresolutionX data from files
    def read_spatialresolutionX_data(file_path, data_list):
        with open(file_path, 'r') as file:
            for line in file:
                if line.startswith('#'):
                    continue  # Skip comment lines
                try:
                    run_number, threshold = map(int, line.strip().split()[:2])
                    output_file_name = f"/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202412/output_{run_number}/analysis.root"
                    root_file = ROOT.TFile.Open(output_file_name)
                    if root_file and not root_file.IsZombie():
                        spatialresolutionX_hist = root_file.Get("AnalysisDUT/H2M_0/residualsTime")
                        if spatialresolutionX_hist:
                            #spatialresolutionX_hist.GetXaxis().SetRangeUser(-50, 50)
                            mean = spatialresolutionX_hist.GetMean()
                            rms = spatialresolutionX_hist.GetRMS()
                            #spatialresolutionX_hist.GetXaxis().SetRangeUser(mean - 3*rms, mean + 3*rms)
                            spatialresolutionX = spatialresolutionX_hist.GetRMS()
                            spatialresolutionX_error = spatialresolutionX_hist.GetRMSError()
                            data_list.append((run_number, threshold, spatialresolutionX, spatialresolutionX_error))
                            print(file_path, ' run number: ', run_number, " THL: ", threshold, " residual X:", spatialresolutionX)
                        root_file.Close()
                except Exception as e:
                    print(f"Error reading spatialresolutionX data: {e}")


    # Read spatialresolutionX data from each file
    read_spatialresolutionX_data("run_number_toa_1V2.txt", data_spatialresolutionX_1V2)
    read_spatialresolutionX_data("run_number_toa_3V6.txt", data_spatialresolutionX_3V6)


    # Plot setup
    fig, ax1 = plt.subplots()

    # Function to prepare and plot spatialresolutionX data
    def plot_spatialresolutionX_data(data_list, color, marker, label):
        thresholds = [(d[1] - baseline) * calibration_factor for d in data_list]
        threshold_errors = [(d[1] - baseline) * calibration_error for d in data_list]
        spatialresolutionXs = [(np.sqrt(d[2]*d[2]-3.42*3.42)) for d in data_list]
        errors= [d[3] for d in data_list]
        ax1.errorbar(thresholds, spatialresolutionXs, xerr = threshold_errors, yerr=errors, fmt=marker, color=color, label=f"{label}", capsize=3)

    # Plot spatialresolutionX data
    plot_spatialresolutionX_data(data_spatialresolutionX_1V2, color="red", marker="o-", label="-1.2 V")
    plot_spatialresolutionX_data(data_spatialresolutionX_3V6, color="green", marker="s-", label="-3.6 V")


    # Plot grey line for simulations
    #ax1.errorbar(thr_sim, eff_sim, yerr=eff_error_sim, color='grey', label="Simulation -3.6 V", marker = 'D')


    # Customize the left y-axis for spatialresolutionX
    ax1.set_xlabel("Threshold [e-]", fontsize=16)
    ax1.set_ylabel("Time resolution [ns]", fontsize=16, color="black")
    ax1.tick_params(axis="y", labelcolor="black")
    ax1.set_xlim(0, 1000)
    ax1.set_ylim(28, 50)
    ax1.legend(loc="upper right", frameon=False, fontsize=12)

    # Enable and customize minor ticks
    ax1.minorticks_on()
    ax1.tick_params(axis='both', which='major', length=6, width=1.5)  # Customize major ticks
    ax1.tick_params(axis='both', which='minor', length=3, width=1)    # Customize minor ticks



    # Manually add minor ticks
    #ax2.yaxis.set_minor_locator(ticker.LogLocator(subs="auto", base=10.0, numticks=12))
    #ax2.tick_params(axis="y", which="minor", length=3, width=0.5)  # Customize minor ticks
    # Save and show plot
    #fig.tight_layout()
    plt.savefig("timeresolution_vs_threshold_compare_bias.pdf")
    plt.show()

# Run the function
if __name__ == "__main__":
    plot_timeresolution_vs_threshold_compare_bias()
