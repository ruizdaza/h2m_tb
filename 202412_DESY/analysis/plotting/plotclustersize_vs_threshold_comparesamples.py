import ROOT
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as ticker

def plot_clustersize_vs_threshold_compare_bias():
    baseline_h2m8 = 99.5
    baseline_h2m2 = 99
    baseline_h2m10 = 97.6
    calibration_factor = 32
    calibration_error = 1

    # Initialize data storage for each bias voltage
    data_clustersize_1V2, data_clustersize_3V6, data_clustersize_2V4 = [], [], []
    data_fake_rate_1V2, data_fake_rate_3V6, data_fake_rate_2V4 = [], [], []

    # Function to read clustersize data from files
    def read_clustersize_data_h2m8(file_path, data_list):
        with open(file_path, 'r') as file:
            for line in file:
                if line.startswith('#'):
                    continue  # Skip comment lines
                try:
                    run_number, threshold = map(int, line.strip().split()[:2])
                    output_file_name = f"/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/output_{run_number}/analysis.root"
                    root_file = ROOT.TFile.Open(output_file_name)
                    if root_file and not root_file.IsZombie():
                        clustersize_hist = root_file.Get("AnalysisDUT/H2M_0/clusterSizeAssociated")
                        if clustersize_hist:
                            clustersize = clustersize_hist.GetMean()
                            clustersize_error = clustersize_hist.GetMeanError()
                            data_list.append((run_number, threshold, clustersize, clustersize_error))
                            print(file_path, ' run number: ', run_number, " THL: ", threshold, " cluster size:", clustersize)
                        root_file.Close()
                except Exception as e:
                    print(f"Error reading clustersize data h2m8: {e}")
    def read_clustersize_data_h2m10(file_path, data_list):
        with open(file_path, 'r') as file:
            for line in file:
                if line.startswith('#'):
                    continue  # Skip comment lines
                try:
                    run_number, threshold = map(int, line.strip().split()[:2])
                    output_file_name = f"/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202412/output_{run_number}/analysis.root"
                    root_file = ROOT.TFile.Open(output_file_name)
                    if root_file and not root_file.IsZombie():
                        clustersize_hist = root_file.Get("AnalysisDUT/H2M_0/clusterSizeAssociated")
                        if clustersize_hist:
                            clustersize = clustersize_hist.GetMean()
                            clustersize_error = clustersize_hist.GetMeanError()
                            data_list.append((run_number, threshold, clustersize, clustersize_error))
                            print(file_path, ' run number: ', run_number, " THL: ", threshold, " cluster size:", clustersize)
                        root_file.Close()
                except Exception as e:
                    print(f"Error reading clustersize data h2m10: {e}")

    def read_clustersize_data_h2m2(file_path, data_list):
        with open(file_path, 'r') as file:
            for line in file:
                if line.startswith('#'):
                    continue  # Skip comment lines
                try:
                    run_number, threshold = map(int, line.strip().split()[:2])
                    output_file_name = f"/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202404/fakehitrate/output_{run_number}/analysis.root"
                    root_file = ROOT.TFile.Open(output_file_name)
                    if root_file and not root_file.IsZombie():
                        clustersize_hist = root_file.Get("AnalysisDUT/H2M_0/clusterSizeAssociated")
                        if clustersize_hist:
                            clustersize = clustersize_hist.GetMean()
                            clustersize_error = clustersize_hist.GetMeanError()
                            data_list.append((run_number, threshold, clustersize, clustersize_error))
                            print(file_path, ' run number: ', run_number, " THL: ", threshold, " cluster size:", clustersize)
                        root_file.Close()
                except Exception as e:
                    print(f"Error reading clustersize data h2m2: {e}")

    # Read clustersize data from each file
    read_clustersize_data_h2m8("run_number_1V2_triggered_h2m8.txt", data_clustersize_1V2)
    read_clustersize_data_h2m2("run_number_1V2_triggered_h2m2.txt", data_clustersize_2V4)
    read_clustersize_data_h2m10("run_number_1V2_triggered_h2m10.txt", data_clustersize_3V6)



    # Plot setup
    fig, ax1 = plt.subplots()

    # Function to prepare and plot clustersize data
    def plot_clustersize_data_h2m8(data_list, color, marker, label):
        thresholds = [(d[1] - baseline_h2m8) * calibration_factor for d in data_list]
        threshold_errors = [(d[1] - baseline_h2m8) * calibration_error for d in data_list]
        clustersizes = [d[2] for d in data_list]
        errors= [d[3] for d in data_list]
        ax1.errorbar(thresholds, clustersizes, xerr = threshold_errors, yerr=errors, fmt=marker, color=color, label=f"{label}", capsize=3)

    def plot_clustersize_data_h2m10(data_list, color, marker, label):
        thresholds = [(d[1] - baseline_h2m10) * calibration_factor for d in data_list]
        threshold_errors = [(d[1] - baseline_h2m10) * calibration_error for d in data_list]
        clustersizes = [d[2] for d in data_list]
        errors= [d[3] for d in data_list]
        ax1.errorbar(thresholds, clustersizes, xerr = threshold_errors, yerr=errors, fmt=marker, color=color, label=f"{label}", capsize=3)

    def plot_clustersize_data_h2m2(data_list, color, marker, label):
        thresholds = [(d[1] - baseline_h2m2) * calibration_factor for d in data_list]
        threshold_errors = [(d[1] - baseline_h2m2) * calibration_error for d in data_list]
        clustersizes = [d[2] for d in data_list]
        errors= [d[3] for d in data_list]
        ax1.errorbar(thresholds, clustersizes, xerr = threshold_errors, yerr=errors, fmt=marker, color=color, label=f"{label}", capsize=3)

    # Plot clustersize data
    plot_clustersize_data_h2m10(data_clustersize_3V6, color="darksalmon", marker="s-", label="21 $\mu$m")
    plot_clustersize_data_h2m8(data_clustersize_1V2, color="red", marker="o-", label="25 $\mu$m")
    plot_clustersize_data_h2m2(data_clustersize_2V4, color="maroon", marker="^-", label="50 $\mu$m")

    # Plot grey line for simulations
    #ax1.errorbar(thr_sim, eff_sim, yerr=eff_error_sim, color='grey', label="Simulation -3.6 V", marker = 'D')


    # Customize the left y-axis for clustersize
    ax1.set_xlabel("Threshold [e-]", fontsize=16)
    ax1.set_ylabel("Cluster size [px]", fontsize=16, color="black")
    ax1.tick_params(axis="y", labelcolor="black")
    ax1.set_xlim(0, 1000)
    ax1.set_ylim(0.75, 1.8)
    ax1.legend(loc="upper right", frameon=False, fontsize=12)

    # Enable and customize minor ticks
    ax1.minorticks_on()
    ax1.tick_params(axis='both', which='major', length=6, width=1.5)  # Customize major ticks
    ax1.tick_params(axis='both', which='minor', length=3, width=1)    # Customize minor ticks



    # Manually add minor ticks
    #ax2.yaxis.set_minor_locator(ticker.LogLocator(subs="auto", base=10.0, numticks=12))
    #ax2.tick_params(axis="y", which="minor", length=3, width=0.5)  # Customize minor ticks
    # Save and show plot
    #fig.tight_layout()
    plt.savefig("clustersize_vs_threshold_compare_samples.pdf")
    plt.show()

# Run the function
if __name__ == "__main__":
    plot_clustersize_vs_threshold_compare_bias()
