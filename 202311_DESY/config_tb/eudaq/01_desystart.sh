# IP-environment variables are set by user/eudet/misc/environments/setup_eudaq2_aida-tlu.sh 
# Define port
export RUNCONTROLIP=192.168.22.1
export RPCPORT=44000

killall xterm

# Start Run Control
xterm -T "Run Control" -e '~/software/eudaq/bin/euRun' &
sleep 2

# Start Logger
 xterm -T "Log Collector" -e '~/software/eudaq/bin/euLog -r tcp://${RUNCONTROLIP}' &
sleep 1

# Start one DataCollector
# name (-t) in conf file
# or: -n TriggerIDSyncDataCollector
xterm -T "Data Collector H2M" -e '~/software/eudaq/bin/euCliCollector -n DirectSaveDataCollector -t >
xterm -T "Data Collector ADENIUM" -e '/opt/eudaq2/bin/euCliCollector -n DirectSaveDataCollector -t t>
#xterm -T "Data Collector ADENIUM" -e '/opt/eudaq2/bin/euCliCollector -n TriggerIDSyncDataCollector >
sleep 1

# Start TLU Producer
xterm -T "AidaTluProducer" -e 'euCliProducer -n AidaTluProducer -t aida_tlu -r tcp://${RUNCONTROLIP}>
sleep 1

# Adenium
xterm -T "Adenium" -e '/opt/eudaq2/bin/euCliProducer -n AltelNiProducer -t altel -r tcp://${RUNCONTR>
sleep 1

xterm -T "H2M Producer" -e 'ssh root@192.168.22.137 "euCliProducer -n CaribouProducer -t H2M -r tcp:>


