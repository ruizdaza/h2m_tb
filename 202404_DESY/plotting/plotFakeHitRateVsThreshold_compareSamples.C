#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TH1D.h>

void plotFakeHitRateVsThreshold_compareSamples() {
    double baseline = 99;
    std::vector<std::tuple<int, int, double, double>> data1V2; // Store data for -1.2V
    std::vector<std::tuple<int, int, double, double>> data0V;   // Store data for 0V
    std::vector<std::tuple<int, int, double, double>> data2V4; // Store data for -2.4V
    std::vector<std::tuple<int, int, double, double>> data3V6; // Store data for -3.6V

    // Open and read data from run_number_1V2.txt
    std::ifstream inputFile1V2("run_number_1V2_tot_h2m2.txt");
    if (!inputFile1V2.is_open()) {
        std::cerr << "Failed to open run_number_1V2_tot_h2m2.txt" << std::endl;
        return;
    }

    std::string line;
    while (std::getline(inputFile1V2, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (1V2): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202404/output_%d/analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the fake-hit rate histogram
        TH1D* fakeRateHist = (TH1D*)f->Get("AnalysisEfficiency/H2M_0/fake_rate/hFakePixelPerEvent");

        if (fakeRateHist) {
            double mean = fakeRateHist->GetMean();
            double error = fakeRateHist->GetRMS(); // Use RMS as an estimate of the error

            std::cout << "Fake-Hit Rate (1V2): " << mean << std::endl;
            std::cout << "Error (1V2): " << error << std::endl;

            // Store the data in the vector
            data1V2.push_back(std::make_tuple(runNumber, threshold, mean, error));
        } else {
            std::cerr << "Failed to get fake-hit rate for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile1V2.close();

    // Open and read data from run_number_0V.txt
  std::ifstream inputFile0V("run_number_1V2_tot_h2m3.txt");
  if (!inputFile0V.is_open()) {
      std::cerr << "Failed to open run_number_1V2_toa_h2m2.txt" << std::endl;
      return;
  }

  while (std::getline(inputFile0V, line)) {
      int runNumber, threshold;

      if (line[0] == '#')
          continue;  // Skip comment lines

      std::istringstream iss(line);
      if (!(iss >> runNumber >> threshold)) {
          std::cerr << "Failed to read run number and threshold" << std::endl;
          continue;
      }
      std::cout << "Run number (0V): " << runNumber << ", Threshold: " << threshold << std::endl;

      // Get the data output file
      std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202404/output_%d/analysis.root", runNumber);

      // Open the ROOT file
      TFile* f = new TFile(outputFileName.c_str());
      if (!f || f->IsZombie()) {
          std::cerr << "Failed to open file: " << outputFileName << std::endl;
          continue;
      }

      // Get the fake-hit rate histogram
      TH1D* fakeRateHist = (TH1D*)f->Get("AnalysisEfficiency/H2M_0/fake_rate/hFakePixelPerEvent");

      if (fakeRateHist) {
          double mean = fakeRateHist->GetMean();
          double error = fakeRateHist->GetRMS(); // Use RMS as an estimate of the error

          std::cout << "Fake-Hit Rate (0V): " << mean << std::endl;
          std::cout << "Error (0V): " << error << std::endl;

          // Store the data in the vector
          data0V.push_back(std::make_tuple(runNumber, threshold, mean, error));
      } else {
          std::cerr << "Failed to get fake-hit rate for Run " << runNumber << std::endl;
      }

      // Close the ROOT file
      f->Close();
  }

// Close the input file
inputFile0V.close();

    // Create a TCanvas for plotting
    TCanvas* canvas = new TCanvas("canvas", "Fake-Hit Rate vs Threshold", 800, 600);

    // Plot data from run_number_1V2.txt in red
    int nDataPoints1V2 = data1V2.size();
    double thresholds1V2[nDataPoints1V2];
    double fakeRates1V2[nDataPoints1V2];
    double fakeRateErrors1V2[nDataPoints1V2];

    for (int i = 0; i < nDataPoints1V2; ++i) {
        thresholds1V2[i] = std::get<1>(data1V2[i]); // Threshold
        fakeRates1V2[i] = std::get<2>(data1V2[i]); // Fake-Hit Rate
        fakeRateErrors1V2[i] = std::get<3>(data1V2[i]); // Error
    }

    TGraphErrors* graph1V2 = new TGraphErrors(nDataPoints1V2, thresholds1V2, fakeRates1V2, 0, fakeRateErrors1V2);
    graph1V2->SetTitle("");
    graph1V2->GetXaxis()->SetTitle("Threshold [DAC]");
    graph1V2->GetYaxis()->SetTitle("Fake-Hit Rate [pixel/event]");
    TAxis *axisX = graph1V2->GetXaxis();
    axisX->SetLimits(baseline,135); //setrangeuser in x
    graph1V2->GetHistogram()->SetMaximum(25);   // setrangeuser in y

    graph1V2->SetMarkerStyle(20);
    graph1V2->SetMarkerColor(kRed);
    graph1V2->SetLineColor(kRed);
    graph1V2->SetLineWidth(2);
    graph1V2->GetXaxis()->SetLabelSize(0.04);
    graph1V2->GetYaxis()->SetLabelSize(0.04);
    graph1V2->GetXaxis()->SetTitleSize(0.055);
    graph1V2->GetYaxis()->SetTitleSize(0.055);
    graph1V2->GetXaxis()->SetTitleOffset(0.85);
    graph1V2->GetYaxis()->SetTitleOffset(0.85);
    graph1V2->GetXaxis()->SetLabelSize(0.05);
    graph1V2->GetYaxis()->SetLabelSize(0.05);
    graph1V2->Draw("APL");

    // Create TGraphErrors and add entries to the legend for 0V
    int nDataPoints0V = data0V.size();
    double thresholds0V[nDataPoints0V];
    double fakeRates0V[nDataPoints0V];
    double fakeRateErrors0V[nDataPoints0V];

    for (int i = 0; i < nDataPoints0V; ++i) {
        thresholds0V[i] = std::get<1>(data0V[i])+4; // Threshold
        fakeRates0V[i] = std::get<2>(data0V[i]); // Fake-Hit Rate
        fakeRateErrors0V[i] = std::get<3>(data0V[i]); // Error
    }

    TGraphErrors* graph0V = new TGraphErrors(nDataPoints0V, thresholds0V, fakeRates0V, 0, fakeRateErrors0V);
    graph0V->SetMarkerStyle(23);
    graph0V->SetMarkerColor(kBlack);
    graph0V->SetLineColor(kBlack);
    graph0V->SetLineWidth(2);
    graph0V->GetXaxis()->SetLabelSize(0.04);
    graph0V->GetYaxis()->SetLabelSize(0.04);
    graph0V->GetXaxis()->SetTitleSize(0.055);
    graph0V->GetYaxis()->SetTitleSize(0.055);
    graph0V->GetXaxis()->SetTitleOffset(0.85);
    graph0V->GetYaxis()->SetTitleOffset(0.85);
    graph0V->GetXaxis()->SetLabelSize(0.05);
    graph0V->GetYaxis()->SetLabelSize(0.05);
    graph0V->Draw("PL");

    // Create a legend with all entries
    TLegend* legend = new TLegend(0.6, 0.6, 0.85, 0.85);
    legend->AddEntry(graph1V2, "H2M-2", "lp");
    legend->AddEntry(graph0V, "H2M-3", "lp");

    // Add entries for other voltage values
    legend->SetBorderSize(0);
    legend->Draw();


        // Create a new X-axis for electrons
        double electronRangeMin = (baseline - baseline) * 40;
        double electronRangeMax = (135 - baseline) * 40;
        TGaxis* axisTop = new TGaxis(baseline, 25, 135, 25, electronRangeMin, electronRangeMax, 510, "-L");
        axisTop->SetTitle("Threshold [estimated electrons]");
        axisTop->SetLabelSize(0.05);  // Adjust label size
        axisTop->SetTitleSize(0.055);  // Adjust title size
        axisTop->SetTitleOffset(0.85);  // Adjust title offset
        axisTop->SetLabelFont(42); // Set font style for axis labels
        axisTop->SetTitleFont(42); // Set font style for axis title
        axisTop->Draw();


        // Compute the ratio between ToT and ToA
        std::vector<double> thresholdsRatio;
        std::vector<double> ratioValues;
        for (int i = 0; i < nDataPoints1V2; ++i) {
            for (int j = 0; j < nDataPoints0V; ++j) {
                if (thresholds1V2[i] == thresholds0V[j]) { // Matching threshold
                    double ratio = fakeRates1V2[i] / fakeRates0V[j];
                    thresholdsRatio.push_back(thresholds0V[j]);
                    ratioValues.push_back(ratio);
                    break;
                }
            }
        }

        // Create a new TGraphErrors for the ratio plot
        TGraph* graphRatio = new TGraph(thresholdsRatio.size(), &thresholdsRatio[0], &ratioValues[0]);
        graphRatio->SetMarkerStyle(21); // Choose the marker style you prefer
        graphRatio->SetMarkerColor(kBlue); // Set the marker color
        graphRatio->SetLineColor(kBlue); // Set the line color
        graphRatio->SetLineWidth(2);

        // Adjust the bottom margin of the canvas to make space for the ratio plot
        double bottomMargin = 0.7; // Adjust the bottom margin as needed
        canvas->SetBottomMargin(0.3);

        // Calculate the top position of the ratio plot
        double topPos = 0.9 - bottomMargin;
        double bottomPos = 0.0;
        double ratioHeight = (bottomMargin - bottomPos) * 0.8; // Adjust the height of the ratio plot as needed

        // Create a new TPad for the ratio plot
        TPad* ratioPad = new TPad("ratioPad", "Ratio Pad", 0, bottomPos, 0.95, topPos);
        ratioPad->SetTopMargin(0);
        ratioPad->SetBottomMargin(0.25);
        ratioPad->SetLeftMargin(0.1);
        ratioPad->SetRightMargin(0.05);
        ratioPad->Draw();
        ratioPad->cd();

        // Draw the ratio plot
        graphRatio->SetTitle("");
        //graphRatio->GetXaxis()->SetTitle("Threshold [DAC]");
        graphRatio->GetYaxis()->SetTitle("H2M-2 / H2M-3");
        graphRatio->GetXaxis()->SetTitleSize(0);  // Adjust title size
        graphRatio->GetYaxis()->SetTitleSize(0.15);
        graphRatio->GetXaxis()->SetTitleOffset(0.83);  // Adjust title offset
        graphRatio->GetYaxis()->SetTitleOffset(0.3);
        graphRatio->GetXaxis()->SetLabelSize(0);  // Adjust axis number size
        graphRatio->GetYaxis()->SetLabelSize(0.15);
        graphRatio->Draw("APL");

        // Add a discontinuous grey line at ratio = 1
        TLine* lineRatioOne = new TLine(baseline, 1, 135, 1);
        lineRatioOne->SetLineStyle(2);
        lineRatioOne->SetLineColor(kGray);
        lineRatioOne->Draw();

        // Set the same x-axis range as the efficiency plots
        double minX = baseline;
        double maxX = 135;
        graphRatio->GetXaxis()->SetLimits(minX, maxX); // Set the same range as efficiency plots
        graphRatio->GetXaxis()->SetRangeUser(minX, maxX); // Set the same range as efficiency plots
        graphRatio->GetYaxis()->SetNdivisions(4);
        graphRatio->GetXaxis()->SetTickLength(0.15);





        // Return to the main canvas
        canvas->cd();
        canvas->Update();
    //canvas->SaveAs("save_fake_hit_rate_vs_threshold.C");
    canvas->SaveAs("save_fake_hit_rate_vs_threshold_compareSamples.pdf");
}

int main() {
    plotFakeHitRateVsThreshold_compareSamples();
    return 0;
}
