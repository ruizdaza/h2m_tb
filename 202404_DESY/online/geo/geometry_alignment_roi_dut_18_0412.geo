[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.62267deg,0.749601deg,0.771946deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.03085mm,-303.729um,0um
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.67911deg,1.17926deg,-0.0506495deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -103.253um,12.018um,24mm
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.98255deg,-0.781858deg,0.177617deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 169.872um,-428.94um,51mm
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.0379871deg,0.0357526deg,-0.822137deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -730.222um,1.23861mm,96mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
roi = [[350,250],[350,380],[580,380],[580,250]]
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.36605deg,-0.49177deg,0.141521deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 609.088um,90.204um,149mm
roi = [[350,250],[350,380],[580,380],[580,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.59362deg,0.677007deg,0.184893deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 626.545um,-63.124um,175mm
roi = [[350,250],[350,380],[580,380],[580,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

