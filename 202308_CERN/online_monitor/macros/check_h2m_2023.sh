#!/bin/bash


CORRY=/opt/corryvreckan
RUN=${1}

# Set up the arguments to be passed to Corryvreckan
GEOMETRY=../cond/Alignment_August2016_Run${RUN}.conf
GEOMETRY="../cond/test_h2m.geo"
GEOMETRY_NEW="../cond/temp.geo"
HISTOGRAMFILE=../output/histograms_${RUN}.root
datapath=/tbdata/tbAugust2023/h2m
INPUTFILE_H2M=$(find $datapath -name "run*${RUN}_*" )                                                                                          
INPUTDIR_TPX=/tbdata/tbAugust2023/data/Run${RUN}


echo "--- Input H2M: "  ${INPUTFILE_H2M}
echo "--- Input tpx3: " ${INPUTDIR_TPX}
${CORRY}/bin/corry -c telescope_tpx3_2023_h2m.conf \
                   -o detectors_file=${GEOMETRY} \
                   -o detectors_file_updated=${GEOMETRY_NEW} \
                   -o histogram_file=${HISTOGRAMFILE} \
		   -o EventLoaderEUDAQ2.file_name=${INPUTFILE_H2M} \
		   -o EventLoaderTimestamp.input_directory=${INPUTDIR_TPX} \
                   -o EventLoaderTimepix3.input_directory=${INPUTDIR_TPX} \
                   -o OnlineMonitor.canvas_title="\"CLICdp Online Monitor - Run ${1}\""

