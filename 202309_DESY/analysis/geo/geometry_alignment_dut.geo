[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.0484722deg,-0.144099deg,0.445589deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 931.137um,45.418um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.431896deg,1.49702deg,-0.143469deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 339.023um,469.63um,98mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.715796deg,1.81015deg,0.121123deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 88.878um,78.102um,124mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = -0.00974028deg,-0.0182201deg,-0.650364deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 2.05131mm,74.151um,162mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,193mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.03579deg,-1.28595deg,0.115623deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 326.863um,82.913um,218mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 2.23683deg,2.06803deg,0.0426854deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 694.122um,-92.463um,305mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

