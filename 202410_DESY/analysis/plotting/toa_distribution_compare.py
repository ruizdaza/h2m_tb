import ROOT
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

# Function to rebin histograms
def rebin(hist, factor):
    if hist.GetNbinsX() % factor != 0:
        raise ValueError("Number of bins must be divisible by the rebinning factor.")

    # Calculate new bin counts and edges
    values = np.array([hist.GetBinContent(i+1) for i in range(hist.GetNbinsX())])
    bin_edges = np.array([hist.GetBinLowEdge(i+1) for i in range(1, hist.GetNbinsX()+2)])
    rebinned_values = values.reshape(-1, factor).sum(axis=1)
    rebinned_edges = bin_edges[::factor]

    if len(rebinned_edges) == len(rebinned_values) + 1:
        return rebinned_values, rebinned_edges
    else:
        return rebinned_values, np.append(rebinned_edges, bin_edges[-1])

# Open ROOT files and extract histograms
file_3V6_ikrum10 = ROOT.TFile.Open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/h2m8_3V6_ikrum10_toa.root")
file_3V6_ikrum10_thr107 = ROOT.TFile.Open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/h2m8_3V6_ikrum10_toa_thr107.root")
file_3V6_ikrum21 = ROOT.TFile.Open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/h2m8_3V6_ikrum21_toa.root")
file_1V2_ikrum10 = ROOT.TFile.Open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/h2m8_1V2_ikrum10_toa.root")
file_1V2_ikrum21 = ROOT.TFile.Open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/h2m8_1V2_ikrum21_toa.root")

h_3V6_ikrum10 = file_3V6_ikrum10.Get("AnalysisDUT/H2M_0/residualsTime")
h_3V6_ikrum10_thr107 = file_3V6_ikrum10_thr107.Get("AnalysisDUT/H2M_0/residualsTime")
h_3V6_ikrum21 = file_3V6_ikrum21.Get("AnalysisDUT/H2M_0/residualsTime")
h_1V2_ikrum10 = file_1V2_ikrum10.Get("AnalysisDUT/H2M_0/residualsTime")
h_1V2_ikrum21 = file_1V2_ikrum21.Get("AnalysisDUT/H2M_0/residualsTime")

# Calculate RMS values
rms_3V6_ikrum10 = h_3V6_ikrum10.GetRMS()
rms_3V6_ikrum10_thr107 = h_3V6_ikrum10_thr107.GetRMS()
rms_3V6_ikrum21 = h_3V6_ikrum21.GetRMS()
rms_1V2_ikrum10 = h_1V2_ikrum10.GetRMS()
rms_1V2_ikrum21 = h_1V2_ikrum21.GetRMS()

# Rebin histograms and normalize
factor = 100
values_3V6_ikrum10, bin_edges_3V6_ikrum10 = rebin(h_3V6_ikrum10, factor)
values_3V6_ikrum10_thr107, bin_edges_3V6_ikrum10_thr107 = rebin(h_3V6_ikrum10_thr107, factor)
values_3V6_ikrum21, bin_edges_3V6_ikrum21 = rebin(h_3V6_ikrum21, factor)
values_1V2_ikrum10, bin_edges_1V2_ikrum10 = rebin(h_1V2_ikrum10, factor)
values_1V2_ikrum21, bin_edges_1V2_ikrum21 = rebin(h_1V2_ikrum21, factor)

# Calculate bin centers and normalize
def normalize(values, bin_centers):
    norm = np.sum(values) * (bin_centers[1] - bin_centers[0])
    return values / norm

bin_centers_3V6_ikrum10 = (bin_edges_3V6_ikrum10[:-1] + bin_edges_3V6_ikrum10[1:]) / 2
values_3V6_ikrum10 = normalize(values_3V6_ikrum10, bin_centers_3V6_ikrum10)

bin_centers_3V6_ikrum10_thr107 = (bin_edges_3V6_ikrum10_thr107[:-1] + bin_edges_3V6_ikrum10_thr107[1:]) / 2
values_3V6_ikrum10_thr107 = normalize(values_3V6_ikrum10_thr107, bin_centers_3V6_ikrum10_thr107)

bin_centers_3V6_ikrum21 = (bin_edges_3V6_ikrum21[:-1] + bin_edges_3V6_ikrum21[1:]) / 2
values_3V6_ikrum21 = normalize(values_3V6_ikrum21, bin_centers_3V6_ikrum21)

bin_centers_1V2_ikrum10 = (bin_edges_1V2_ikrum10[:-1] + bin_edges_1V2_ikrum10[1:]) / 2
values_1V2_ikrum10 = normalize(values_1V2_ikrum10, bin_centers_1V2_ikrum10)

bin_centers_1V2_ikrum21 = (bin_edges_1V2_ikrum21[:-1] + bin_edges_1V2_ikrum21[1:]) / 2
values_1V2_ikrum21 = normalize(values_1V2_ikrum21, bin_centers_1V2_ikrum21)

# Plot with RMS in legend
plt.figure(figsize=(8, 6))

#plt.step(bin_centers_3V6_ikrum21, values_3V6_ikrum21, color='black', label=f'3.6 V, ikrum 21 (RMS={rms_3V6_ikrum21:.1f} ns)', linewidth=2)
#plt.step(bin_centers_1V2_ikrum21, values_1V2_ikrum21, color='red', label=f'1.2 V, ikrum 21 (RMS={rms_1V2_ikrum21:.1f} ns)', linewidth=2)
#plt.step(bin_centers_3V6_ikrum10, values_3V6_ikrum10, color='green', label=f'3.6 V, ikrum 10 (RMS={rms_3V6_ikrum10:.1f} ns)', linewidth=2)
#plt.step(bin_centers_1V2_ikrum10, values_1V2_ikrum10, color='blue', label=f'1.2 V, ikrum 10 (RMS={rms_1V2_ikrum10:.1f} ns)', linewidth=2)

plt.step(bin_centers_3V6_ikrum10, values_3V6_ikrum10, color='green', label=f'THL 288 e- (RMS={rms_3V6_ikrum10:.1f} ns)', linewidth=2)
plt.step(bin_centers_3V6_ikrum10_thr107, values_3V6_ikrum10_thr107, color='lime', label=f'THL 224 e- (RMS={rms_3V6_ikrum10_thr107:.1f} ns)', linewidth=2)


# Labels and limits
plt.xlabel("$t_{trigger} - t_{hit}$ [ns]", fontsize=16)
plt.ylabel("Normalized Events", fontsize=16)
plt.ylim(0, 0.025)
plt.xlim(-100, 200)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)


# Legend
plt.legend(fontsize=14, frameon=False)

# Save plot
output_pdf_path = "toa_distribution_thr.pdf"
plt.savefig(output_pdf_path)
plt.close()

print(f"Plot saved as {output_pdf_path}")
