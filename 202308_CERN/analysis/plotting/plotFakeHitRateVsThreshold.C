#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraphErrors.h>
#include <TGraph.h>
#include <TLegend.h>
#include <TH1D.h>

void plotFakeHitRateVsThreshold() {
    std::vector<std::tuple<int, int, double, double>> data1V2; // Store data for -1.2V
    std::vector<std::tuple<int, int, double, double>> data0V;   // Store data for 0V
    std::vector<std::tuple<int, int, double, double>> data2V4; // Store data for -2.4V
    std::vector<std::tuple<int, int, double, double>> data3V6; // Store data for -3.6V

    // Open and read data from run_number_1V2.txt
    std::ifstream inputFile1V2("run_number_1V2.txt");
    if (!inputFile1V2.is_open()) {
        std::cerr << "Failed to open run_number_1V2.txt" << std::endl;
        return;
    }

    std::string line;
    while (std::getline(inputFile1V2, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (1V2): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the fake-hit rate histogram
        TH1D* fakeRateHist = (TH1D*)f->Get("AnalysisEfficiency/H2M_0/fake_rate/hFakePixelPerEvent");

        if (fakeRateHist) {
            double mean = fakeRateHist->GetMean();
            double error = fakeRateHist->GetRMS(); // Use RMS as an estimate of the error

            std::cout << "Fake-Hit Rate (1V2): " << mean << std::endl;
            std::cout << "Error (1V2): " << error << std::endl;

            // Store the data in the vector
            data1V2.push_back(std::make_tuple(runNumber, threshold, mean, error));
        } else {
            std::cerr << "Failed to get fake-hit rate for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile1V2.close();



    // Open and read data from run_number_2V4.txt
    std::ifstream inputFile2V4("run_number_2V4.txt");
    if (!inputFile2V4.is_open()) {
        std::cerr << "Failed to open run_number_2V4.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile2V4, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (2V4): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the fake-hit rate histogram
        TH1D* fakeRateHist = (TH1D*)f->Get("AnalysisEfficiency/H2M_0/fake_rate/hFakePixelPerEvent");

        if (fakeRateHist) {
            double mean = fakeRateHist->GetMean();
            double error = fakeRateHist->GetRMS(); // Use RMS as an estimate of the error

            std::cout << "Fake-Hit Rate (2V4): " << mean << std::endl;
            std::cout << "Error (2V4): " << error << std::endl;

            // Store the data in the vector
            data2V4.push_back(std::make_tuple(runNumber, threshold, mean, error));
        } else {
            std::cerr << "Failed to get fake-hit rate for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile2V4.close();

    // Open and read data from run_number_3V6.txt
    std::ifstream inputFile3V6("run_number_3V6.txt");
    if (!inputFile3V6.is_open()) {
        std::cerr << "Failed to open run_number_3V6.txt" << std::endl;
        return;
    }

    while (std::getline(inputFile3V6, line)) {
        int runNumber, threshold;

        if (line[0] == '#')
            continue;  // Skip comment lines

        std::istringstream iss(line);
        if (!(iss >> runNumber >> threshold)) {
            std::cerr << "Failed to read run number and threshold" << std::endl;
            continue;
        }
        std::cout << "Run number (3V6): " << runNumber << ", Threshold: " << threshold << std::endl;

        // Get the data output file
        std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_%d/dut_analysis.root", runNumber);

        // Open the ROOT file
        TFile* f = new TFile(outputFileName.c_str());
        if (!f || f->IsZombie()) {
            std::cerr << "Failed to open file: " << outputFileName << std::endl;
            continue;
        }

        // Get the fake-hit rate histogram
        TH1D* fakeRateHist = (TH1D*)f->Get("AnalysisEfficiency/H2M_0/fake_rate/hFakePixelPerEvent");

        if (fakeRateHist) {
            double mean = fakeRateHist->GetMean();
            double error = fakeRateHist->GetRMS(); // Use RMS as an estimate of the error

            std::cout << "Fake-Hit Rate (3V6): " << mean << std::endl;
            std::cout << "Error (3V6): " << error << std::endl;

            // Store the data in the vector
            data3V6.push_back(std::make_tuple(runNumber, threshold, mean, error));
        } else {
            std::cerr << "Failed to get fake-hit rate for Run " << runNumber << std::endl;
        }

        // Close the ROOT file
        f->Close();
    }

    // Close the input file
    inputFile3V6.close();

    // Open and read data from run_number_0V.txt
  std::ifstream inputFile0V("run_number_0V.txt");
  if (!inputFile0V.is_open()) {
      std::cerr << "Failed to open run_number_0V.txt" << std::endl;
      return;
  }

  while (std::getline(inputFile0V, line)) {
      int runNumber, threshold;

      if (line[0] == '#')
          continue;  // Skip comment lines

      std::istringstream iss(line);
      if (!(iss >> runNumber >> threshold)) {
          std::cerr << "Failed to read run number and threshold" << std::endl;
          continue;
      }
      std::cout << "Run number (0V): " << runNumber << ", Threshold: " << threshold << std::endl;

      // Get the data output file
      std::string outputFileName = Form("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_%d/dut_analysis.root", runNumber);

      // Open the ROOT file
      TFile* f = new TFile(outputFileName.c_str());
      if (!f || f->IsZombie()) {
          std::cerr << "Failed to open file: " << outputFileName << std::endl;
          continue;
      }

      // Get the fake-hit rate histogram
      TH1D* fakeRateHist = (TH1D*)f->Get("AnalysisEfficiency/H2M_0/fake_rate/hFakePixelPerEvent");

      if (fakeRateHist) {
          double mean = fakeRateHist->GetMean();
          double error = fakeRateHist->GetRMS(); // Use RMS as an estimate of the error

          std::cout << "Fake-Hit Rate (0V): " << mean << std::endl;
          std::cout << "Error (0V): " << error << std::endl;

          // Store the data in the vector
          data0V.push_back(std::make_tuple(runNumber, threshold, mean, error));
      } else {
          std::cerr << "Failed to get fake-hit rate for Run " << runNumber << std::endl;
      }

      // Close the ROOT file
      f->Close();
  }

// Close the input file
inputFile0V.close();

    // Create a TCanvas for plotting
    TCanvas* canvas = new TCanvas("canvas", "Fake-Hit Rate vs Threshold", 800, 600);

    // Plot data from run_number_1V2.txt in red
    int nDataPoints1V2 = data1V2.size();
    double thresholds1V2[nDataPoints1V2];
    double fakeRates1V2[nDataPoints1V2];
    double fakeRateErrors1V2[nDataPoints1V2];

    for (int i = 0; i < nDataPoints1V2; ++i) {
        thresholds1V2[i] = std::get<1>(data1V2[i]); // Threshold
        fakeRates1V2[i] = std::get<2>(data1V2[i]); // Fake-Hit Rate
        fakeRateErrors1V2[i] = std::get<3>(data1V2[i]); // Error
    }

    TGraphErrors* graph1V2 = new TGraphErrors(nDataPoints1V2, thresholds1V2, fakeRates1V2, 0, fakeRateErrors1V2);
    graph1V2->SetTitle("Fake-Hit Rate vs Threshold");
    graph1V2->GetXaxis()->SetTitle("Threshold");
    graph1V2->GetYaxis()->SetTitle("Fake-Hit Rate [pixel/event]");
    TAxis *axisX = graph1V2->GetXaxis();
    axisX->SetLimits(65,110); //setrangeuser in x
    graph1V2->SetMarkerStyle(20);
    graph1V2->SetMarkerColor(kRed);
    graph1V2->SetLineColor(kRed);
    graph1V2->SetLineWidth(2);
    graph1V2->GetXaxis()->SetLabelSize(0.04);
    graph1V2->GetYaxis()->SetLabelSize(0.04);
    graph1V2->GetXaxis()->SetTitleSize(0.055);
    graph1V2->GetYaxis()->SetTitleSize(0.055);
    graph1V2->GetXaxis()->SetTitleOffset(0.85);
    graph1V2->GetYaxis()->SetTitleOffset(0.85);
    graph1V2->GetXaxis()->SetLabelSize(0.05);
    graph1V2->GetYaxis()->SetLabelSize(0.05);
    graph1V2->Draw("APL");

    // Create TGraphErrors and add entries to the legend for -2.4V
    int nDataPoints2V4 = data2V4.size();
    double thresholds2V4[nDataPoints2V4];
    double fakeRates2V4[nDataPoints2V4];
    double fakeRateErrors2V4[nDataPoints2V4];

    for (int i = 0; i < nDataPoints2V4; ++i) {
        thresholds2V4[i] = std::get<1>(data2V4[i]); // Threshold
        fakeRates2V4[i] = std::get<2>(data2V4[i]); // Fake-Hit Rate
        fakeRateErrors2V4[i] = std::get<3>(data2V4[i]); // Error
    }

    TGraphErrors* graph2V4 = new TGraphErrors(nDataPoints2V4, thresholds2V4, fakeRates2V4, 0, fakeRateErrors2V4);
    graph2V4->SetMarkerStyle(21);
    graph2V4->SetMarkerColor(kBlue);
    graph2V4->SetLineColor(kBlue);
    graph2V4->SetLineWidth(2);
    graph2V4->GetXaxis()->SetLabelSize(0.04);
    graph2V4->GetYaxis()->SetLabelSize(0.04);
    graph2V4->GetXaxis()->SetTitleSize(0.055);
    graph2V4->GetYaxis()->SetTitleSize(0.055);
    graph2V4->GetXaxis()->SetTitleOffset(0.85);
    graph2V4->GetYaxis()->SetTitleOffset(0.85);
    graph2V4->GetXaxis()->SetLabelSize(0.05);
    graph2V4->GetYaxis()->SetLabelSize(0.05);
    graph2V4->Draw("PL");

    // Create TGraphErrors and add entries to the legend for -3.6V
    int nDataPoints3V6 = data3V6.size();
    double thresholds3V6[nDataPoints3V6];
    double fakeRates3V6[nDataPoints3V6];
    double fakeRateErrors3V6[nDataPoints3V6];

    for (int i = 0; i < nDataPoints3V6; ++i) {
        thresholds3V6[i] = std::get<1>(data3V6[i]); // Threshold
        fakeRates3V6[i] = std::get<2>(data3V6[i]); // Fake-Hit Rate
        fakeRateErrors3V6[i] = std::get<3>(data3V6[i]); // Error
    }

    TGraphErrors* graph3V6 = new TGraphErrors(nDataPoints3V6, thresholds3V6, fakeRates3V6, 0, fakeRateErrors3V6);
    graph3V6->SetMarkerStyle(22);
    graph3V6->SetMarkerColor(kGreen);
    graph3V6->SetLineColor(kGreen);
    graph3V6->SetLineWidth(2);
    graph3V6->GetXaxis()->SetLabelSize(0.04);
    graph3V6->GetYaxis()->SetLabelSize(0.04);
    graph3V6->GetXaxis()->SetTitleSize(0.055);
    graph3V6->GetYaxis()->SetTitleSize(0.055);
    graph3V6->GetXaxis()->SetTitleOffset(0.85);
    graph3V6->GetYaxis()->SetTitleOffset(0.85);
    graph3V6->GetXaxis()->SetLabelSize(0.05);
    graph3V6->GetYaxis()->SetLabelSize(0.05);
    graph3V6->Draw("PL");

    // Create TGraphErrors and add entries to the legend for 0V
    int nDataPoints0V = data0V.size();
    double thresholds0V[nDataPoints0V];
    double fakeRates0V[nDataPoints0V];
    double fakeRateErrors0V[nDataPoints0V];

    for (int i = 0; i < nDataPoints0V; ++i) {
        thresholds0V[i] = std::get<1>(data0V[i]); // Threshold
        fakeRates0V[i] = std::get<2>(data0V[i]); // Fake-Hit Rate
        fakeRateErrors0V[i] = std::get<3>(data0V[i]); // Error
    }

    TGraphErrors* graph0V = new TGraphErrors(nDataPoints0V, thresholds0V, fakeRates0V, 0, fakeRateErrors0V);
    graph0V->SetMarkerStyle(23);
    graph0V->SetMarkerColor(kOrange);
    graph0V->SetLineColor(kOrange);
    graph0V->SetLineWidth(2);
    graph0V->GetXaxis()->SetLabelSize(0.04);
    graph0V->GetYaxis()->SetLabelSize(0.04);
    graph0V->GetXaxis()->SetTitleSize(0.055);
    graph0V->GetYaxis()->SetTitleSize(0.055);
    graph0V->GetXaxis()->SetTitleOffset(0.85);
    graph0V->GetYaxis()->SetTitleOffset(0.85);
    graph0V->GetXaxis()->SetLabelSize(0.05);
    graph0V->GetYaxis()->SetLabelSize(0.05);
    graph0V->Draw("PL");

    // Create a legend with all entries
    TLegend* legend = new TLegend(0.6, 0.6, 0.85, 0.85);
    legend->AddEntry(graph3V6, "-3.6 V", "lp");
    legend->AddEntry(graph2V4, "-2.4 V", "lp");
    legend->AddEntry(graph1V2, "-1.2 V", "lp");
    legend->AddEntry(graph0V, "0 V", "lp");

    // Add entries for other voltage values
    legend->SetBorderSize(0);
    legend->Draw();
    canvas->SaveAs("save_fake_hit_rate_vs_threshold.C");
}

int main() {
    plotFakeHitRateVsThreshold();
    return 0;
}
