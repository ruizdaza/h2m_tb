import ROOT
import numpy as np
import matplotlib.pyplot as plt

# Load the ROOT file
file_path = "/scratch/ruizdaza/h2m/h2m_tb/202404_DESY/analysis_tot/output_thr106.root"
root_file = ROOT.TFile.Open(file_path)

# Alert if the root file is not found
if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

# Open the plot
tprofile_path = "AnalysisDUT/H2M_0/qvsxmym4p"
tprofile = root_file.Get(tprofile_path)

# Alert if the plot is not found
if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

# Get the number of bins in X and Y
nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Check that we get 140 bins in X and in Y because it is the 4 pixel plot
print('bins in X: ', nx, ' ; bins in Y: ', ny)

# Create an array to store the mean cluster charge values for the full range
# How? the array is extended by repeating the value tprofile.GetBinContent(i, j) a number of times equal to the number of entries
cluster_charge_values_full_range = []
for i in range(0, nx + 1):
    for j in range(0, ny + 1):
        cluster_charge_values_full_range.extend([tprofile.GetBinContent(i, j)] * int(tprofile.GetBinEntries(tprofile.GetBin(i, j))))

# Create an array to store the cluster charge values within the specified range of x bins
cluster_charge_values_range_8_to_18 = []
for i in range(8*2, 18*2 + 1):
    for j in range(0, ny + 1):
        cluster_charge_values_range_8_to_18.extend([tprofile.GetBinContent(i, j)] * int(tprofile.GetBinEntries(tprofile.GetBin(i, j))))
for i in range(70 + 8*2, 70 + 18*2 + 1):
    for j in range(0, ny + 1):
        cluster_charge_values_range_8_to_18.extend([tprofile.GetBinContent(i, j)] * int(tprofile.GetBinEntries(tprofile.GetBin(i, j))))

cluster_charge_values_range_25_to_35 = []
for i in range(25*2, 35*2 + 1):
    for j in range(0, ny + 1):
        cluster_charge_values_range_25_to_35.extend([tprofile.GetBinContent(i, j)] * int(tprofile.GetBinEntries(tprofile.GetBin(i, j))))
for i in range(70 + 25*2, 70 + 35*2 + 1):
    for j in range(0, ny + 1):
        cluster_charge_values_range_25_to_35.extend([tprofile.GetBinContent(i, j)] * int(tprofile.GetBinEntries(tprofile.GetBin(i, j))))

# Calculate total number of events for each range
total_events_full_range = len(cluster_charge_values_full_range)
total_events_range_8_to_18 = len(cluster_charge_values_range_8_to_18)
total_events_range_25_to_35 = len(cluster_charge_values_range_25_to_35)

# Calculate MPV for each range
mpv_full_range = np.argmax(np.bincount(cluster_charge_values_full_range))
mpv_range_8_to_18 = np.argmax(np.bincount(cluster_charge_values_range_8_to_18))
mpv_range_25_to_35 = np.argmax(np.bincount(cluster_charge_values_range_25_to_35))

# Create the histogram
plt.figure(figsize=(8, 6))
plt.hist(cluster_charge_values_full_range, bins=np.arange(0.5, max(cluster_charge_values_full_range) + 1.5, 1), label=f'0 - 35 µm (Number of events: {total_events_full_range}, MPV: {mpv_full_range})', color='red', alpha=0.5)
plt.hist(cluster_charge_values_range_8_to_18, bins=np.arange(0.5, max(cluster_charge_values_range_8_to_18) + 1.5, 1), label=f'8 - 18 µm (Number of events: {total_events_range_8_to_18}, MPV: {mpv_range_8_to_18})', color='blue', alpha=0.5)
plt.hist(cluster_charge_values_range_25_to_35, bins=np.arange(0.5, max(cluster_charge_values_range_25_to_35) + 1.5, 1), label=f'25 - 35 µm (Number of events: {total_events_range_25_to_35}, MPV: {mpv_range_25_to_35})', color='green', alpha=0.5)
plt.xlabel('Mean cluster charge [a.u.]', fontsize=16)
plt.ylabel('Number of events', fontsize=16)
plt.xlim(0, 60)  # Set the limit from 0 to 150 in the x-axis
plt.legend()
plt.tight_layout()

# Save the plot as PDF
plt.savefig("landau.pdf")

