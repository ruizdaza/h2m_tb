import ROOT
import matplotlib.pyplot as plt
import numpy as np

def plot_efficiency_vs_threshold_and_fake_hit_rate_compare_bias():
    baseline = 99.5
    calibration_factor = 32

    # Initialize data storage for each bias voltage
    data_efficiency_1V2, data_efficiency_3V6, data_efficiency_2V4 = [], [], []
    data_fake_rate_1V2, data_fake_rate_3V6, data_fake_rate_2V4 = [], [], []

    # Function to read efficiency data from files
    def read_efficiency_data(file_path, data_list):
        with open(file_path, 'r') as file:
            for line in file:
                if line.startswith('#'):
                    continue  # Skip comment lines
                try:
                    run_number, threshold = map(int, line.strip().split()[:2])
                    output_file_name = f"/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/output_{run_number}/analysis.root"
                    root_file = ROOT.TFile.Open(output_file_name)
                    if root_file and not root_file.IsZombie():
                        eff_hist = root_file.Get("AnalysisEfficiency/H2M_0/eTotalEfficiency")
                        if eff_hist:
                            efficiency = eff_hist.GetEfficiency(1)
                            efficiency_error_low = eff_hist.GetEfficiencyErrorLow(1)
                            efficiency_error_up = eff_hist.GetEfficiencyErrorUp(1)
                            data_list.append((run_number, threshold, efficiency, efficiency_error_low, efficiency_error_up))
                        root_file.Close()
                except Exception as e:
                    print(f"Error reading efficiency data: {e}")

    # Function to read fake hit rate data from files
    def read_fake_rate_data(file_path, data_list):
        with open(file_path, 'r') as file:
            for line in file:
                if line.startswith('#'):
                    continue  # Skip comment lines
                try:
                    run_number, threshold = map(int, line.strip().split()[:2])
                    output_file_name = f"/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/output_{run_number}/analysis.root"
                    root_file = ROOT.TFile.Open(output_file_name)
                    if root_file and not root_file.IsZombie():
                        fake_rate_hist = root_file.Get("AnalysisEfficiency/H2M_0/fake_rate/hFakePixelPerEvent")
                        if fake_rate_hist:
                            mean_fake_rate = fake_rate_hist.GetMean()
                            error_fake_rate = fake_rate_hist.GetRMS()
                            data_list.append((run_number, threshold, mean_fake_rate, error_fake_rate))
                        root_file.Close()
                except Exception as e:
                    print(f"Error reading fake hit rate data: {e}")

    # Read efficiency data from each file
    read_efficiency_data("run_number_thl_1V2.txt", data_efficiency_1V2)
    read_efficiency_data("run_number_thl_3V6.txt", data_efficiency_3V6)
    read_efficiency_data("run_number_thl_2V4.txt", data_efficiency_2V4)

    # Read fake hit rate data from each file
    read_fake_rate_data("run_number_thl_1V2.txt", data_fake_rate_1V2)
    read_fake_rate_data("run_number_thl_3V6.txt", data_fake_rate_3V6)
    read_fake_rate_data("run_number_thl_2V4.txt", data_fake_rate_2V4)

    # Plot setup
    fig, ax1 = plt.subplots()

    # Function to prepare and plot efficiency data
    def plot_efficiency_data(data_list, color, marker, label):
        thresholds = [(d[1] - baseline) * calibration_factor for d in data_list]
        efficiencies = [d[2] for d in data_list]
        errors_low = [d[3] for d in data_list]
        errors_up = [d[4] for d in data_list]
        ax1.errorbar(thresholds, efficiencies, yerr=[errors_low, errors_up], fmt=marker, color=color, label=f"{label}", capsize=3)

    # Read the simulation data from the file
    thr_sim = []
    eff_sim = []
    eff_error_sim = []
    with open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/simulations/output_dig_3-6V/results.txt", 'r') as coeff_36:
        for line in coeff_36:
            if line.startswith('Data'):
                continue  # Skip comment lines
            try:
                values = line.strip().split(',')
                threshold = float(values[0])  # First value is the threshold
                efficiency = float(values[1])/100.  # Second value is the efficiency
                error_efficiency = float(values[2])/100.  # Second value is the efficiency
                thr_sim.append(threshold)
                eff_sim.append(efficiency)
                eff_error_sim.append(error_efficiency)

                print(efficiency)
            except Exception as e:
                print(f"Error reading simulation data: {e}")

    # Plot grey line for simulations
    ax1.errorbar(thr_sim, eff_sim, yerr=eff_error_sim, color='grey', label="Simulation", marker = 'D')


    # Plot efficiency data
    plot_efficiency_data(data_efficiency_1V2, color="red", marker="o-", label="-1.2 V")
    plot_efficiency_data(data_efficiency_2V4, color="blue", marker="^-", label="-2.4 V")
    plot_efficiency_data(data_efficiency_3V6, color="green", marker="s-", label="-3.6 V")

    # Customize the left y-axis for efficiency
    ax1.set_xlabel("Threshold [e-]", fontsize=16)
    ax1.set_ylabel("Efficiency", fontsize=16, color="black")
    ax1.tick_params(axis="y", labelcolor="black")
    ax1.set_xlim(0, 900)
    ax1.set_ylim(0, 1.05)
    ax1.legend(loc="upper right", frameon=False, fontsize=16, bbox_to_anchor=(0.8, 0.4))

    # Enable and customize minor ticks
    ax1.minorticks_on()
    ax1.tick_params(axis='both', which='major', length=6, width=1.5)  # Customize major ticks
    ax1.tick_params(axis='both', which='minor', length=3, width=1)    # Customize minor ticks

    # Secondary y-axis for fake hit rate
    ax2 = ax1.twinx()

    # Function to prepare and plot fake hit rate data
    def plot_fake_rate_data(data_list, color, marker, label):
        thresholds = [(d[1] - baseline) * calibration_factor for d in data_list]
        fake_rates = [d[2] for d in data_list]
        errors_fake = [d[3] for d in data_list]
        ax2.errorbar(thresholds, fake_rates, yerr=errors_fake, fmt=marker, color=color, label=f"Fake Hit Rate {label}", capsize=3)

    # Plot fake hit rate data
    plot_fake_rate_data(data_fake_rate_1V2, color="red", marker="x--", label="-1.2 V")
    plot_fake_rate_data(data_fake_rate_3V6, color="green", marker="d--", label="0 V")
    plot_fake_rate_data(data_fake_rate_2V4, color="blue", marker="h--", label="-2.4 V")


    # Customize the right y-axis for fake hit rate
    ax2.set_ylabel("Fake Hit Rate [pixels/event]", fontsize=16, color="black")
    ax2.tick_params(axis="y", labelcolor="black")
    ax2.set_ylim(-0.5, 25)  # Adjust as needed

    # Enable and customize minor ticks
    ax2.minorticks_on()
    ax2.tick_params(axis='both', which='major', length=6, width=1.5)  # Customize major ticks
    ax2.tick_params(axis='both', which='minor', length=3, width=1)    # Customize minor ticks

    # Save and show plot
    fig.tight_layout()
    plt.savefig("efficiency_vs_threshold_and_fake_hit_rate_compare_bias_withsim.pdf")
    plt.show()

# Run the function
if __name__ == "__main__":
    plot_efficiency_vs_threshold_and_fake_hit_rate_compare_bias()
