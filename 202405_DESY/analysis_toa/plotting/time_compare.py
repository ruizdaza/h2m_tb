import ROOT
import numpy as np
import matplotlib.pyplot as plt

# Load the ROOT file
file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202405/output_3155/analysis.root"
root_file = ROOT.TFile.Open(file_path)

# Alert if the root file is not found
if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

# Open the plot
tprofile_path = "AnalysisDUT/H2M_0/timeDelay_trackPos_TProfile_4p"
tprofile = root_file.Get(tprofile_path)

# Alert if the plot is not found
if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

# Get the number of bins in X and Y
nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Check that we get 140 bins in X and in Y because it is the 4 pixel plot
print('bins in X: ', nx, ' ; bins in Y: ', ny)

# Function to extract cluster charge values
def extract_cluster_charge_values(start_bin, end_bin, ny):
    values = []
    for i in range(start_bin, end_bin + 1):
        for j in range(1, ny + 1):  # bins start from 1
            bin_content = tprofile.GetBinContent(i, j)
            bin_entries = int(tprofile.GetBinEntries(tprofile.GetBin(i, j)))
            values.extend([bin_content] * bin_entries)
    return values

# Extract cluster charge values for the specified ranges
cluster_charge_values_range_0_to_7 = extract_cluster_charge_values(1, 7*2, ny)
cluster_charge_values_range_8_to_18 = extract_cluster_charge_values(8*2, 18*2, ny) + extract_cluster_charge_values(70 + 8*2, 70 + 18*2, ny)
cluster_charge_values_range_19_to_24 = extract_cluster_charge_values(19*2, 24*2, ny) + extract_cluster_charge_values(70 + 19*2, 70 + 24*2, ny)
cluster_charge_values_range_25_to_35 = extract_cluster_charge_values(25*2, 35*2, ny) + extract_cluster_charge_values(70 + 25*2, 70 + 35*2, ny)

print('Minimum value in range 8 to 18:', np.min(cluster_charge_values_range_8_to_18))

# Shift values to make them non-negative
min_value = min(np.min(cluster_charge_values_range_0_to_7), np.min(cluster_charge_values_range_8_to_18), np.min(cluster_charge_values_range_19_to_24), np.min(cluster_charge_values_range_25_to_35))
shift = -min_value if min_value < 0 else 0

# Function to rebin data by a factor of 5
def rebin_data(data, factor):
    rebinned_data = np.add.reduceat(data, np.arange(0, len(data), factor))
    return rebinned_data

# Calculate the raw bin counts (without normalization)
if cluster_charge_values_range_0_to_7:
    shifted_values_range_0_to_7 = np.array(cluster_charge_values_range_0_to_7) + shift
    bin_counts_range_0_to_7 = np.bincount(shifted_values_range_0_to_7.astype(int))
    bin_counts_range_0_to_7 = rebin_data(bin_counts_range_0_to_7, 5)
else:
    bin_counts_range_0_to_7 = np.array([])

if cluster_charge_values_range_8_to_18:
    shifted_values_range_8_to_18 = np.array(cluster_charge_values_range_8_to_18) + shift
    bin_counts_range_8_to_18 = np.bincount(shifted_values_range_8_to_18.astype(int))
    bin_counts_range_8_to_18 = rebin_data(bin_counts_range_8_to_18, 5)
else:
    bin_counts_range_8_to_18 = np.array([])

if cluster_charge_values_range_19_to_24:
    shifted_values_range_19_to_24 = np.array(cluster_charge_values_range_19_to_24) + shift
    bin_counts_range_19_to_24 = np.bincount(shifted_values_range_19_to_24.astype(int))
    bin_counts_range_19_to_24 = rebin_data(bin_counts_range_19_to_24, 5)
else:
    bin_counts_range_19_to_24 = np.array([])

if cluster_charge_values_range_25_to_35:
    shifted_values_range_25_to_35 = np.array(cluster_charge_values_range_25_to_35) + shift
    bin_counts_range_25_to_35 = np.bincount(shifted_values_range_25_to_35.astype(int))
    bin_counts_range_25_to_35 = rebin_data(bin_counts_range_25_to_35, 5)
else:
    bin_counts_range_25_to_35 = np.array([])

# Calculate total number of events for each range
total_events_range_0_to_7 = len(cluster_charge_values_range_0_to_7)
total_events_range_8_to_18 = len(cluster_charge_values_range_8_to_18)
total_events_range_19_to_24 = len(cluster_charge_values_range_19_to_24)
total_events_range_25_to_35 = len(cluster_charge_values_range_25_to_35)

# Calculate MPV for each range
def calculate_mpv(bin_counts, shift):
    if bin_counts.size > 0:
        max_index = np.argmax(bin_counts)
        mpv = round(max_index * 5 - shift, 1)  # Round to one decimal place
    else:
        mpv = None
    return mpv

mpv_range_0_to_7 = calculate_mpv(bin_counts_range_0_to_7, shift)
mpv_range_8_to_18 = calculate_mpv(bin_counts_range_8_to_18, shift)
mpv_range_19_to_24 = calculate_mpv(bin_counts_range_19_to_24, shift)
mpv_range_25_to_35 = calculate_mpv(bin_counts_range_25_to_35, shift)

# Calculate RMS for each range using weighted average method
def calculate_rms_weighted(bin_counts, shift, bin_width):
    if bin_counts.size == 0:
        return None

    bin_centers = np.arange(len(bin_counts)) * bin_width - shift
    values = bin_counts / np.sum(bin_counts)

    mean = np.average(bin_centers, weights=values)
    variance = np.average((bin_centers - mean) ** 2, weights=values)
    rms = np.sqrt(variance)

    return round(rms, 2)  # Round to two decimal places

rms_range_0_to_7 = calculate_rms_weighted(bin_counts_range_0_to_7, shift, 5)
rms_range_8_to_18 = calculate_rms_weighted(bin_counts_range_8_to_18, shift, 5)
rms_range_19_to_24 = calculate_rms_weighted(bin_counts_range_19_to_24, shift, 5)
rms_range_25_to_35 = calculate_rms_weighted(bin_counts_range_25_to_35, shift, 5)

# Plot raw bin counts (without normalization)
plt.figure(figsize=(8, 6))

def plot_rebinned_data(bin_counts, shift, color, label):
    if bin_counts.size > 0:
        x_values = np.arange(len(bin_counts)) * 5 - shift
        plt.bar(x_values, bin_counts, width=5, label=label, color=color, alpha=0.5)

plot_rebinned_data(bin_counts_range_0_to_7, shift, 'orange', f'0 - 7 µm, Events: {total_events_range_0_to_7}, MPV: {mpv_range_0_to_7}, RMS: {rms_range_0_to_7}')
plot_rebinned_data(bin_counts_range_8_to_18, shift, 'blue', f'8 - 18 µm, Events: {total_events_range_8_to_18}, MPV: {mpv_range_8_to_18}, RMS: {rms_range_8_to_18}')
plot_rebinned_data(bin_counts_range_19_to_24, shift, 'red', f'19 - 24 µm, Events: {total_events_range_19_to_24}, MPV: {mpv_range_19_to_24}, RMS: {rms_range_19_to_24}')
plot_rebinned_data(bin_counts_range_25_to_35, shift, 'green', f'25 - 35 µm, Events: {total_events_range_25_to_35}, MPV: {mpv_range_25_to_35}, RMS: {rms_range_25_to_35}')

plt.xlabel(r'$t_{\mathrm{trigger}} - t_{\mathrm{hit}}$ [ns]', fontsize=16)
plt.ylabel('Number of events', fontsize=16)
plt.xlim(-100, 160)  # Set the limit from -100 to 160 in the x-axis
plt.legend(frameon=False)

plt.tight_layout()

# Save the plot as PDF
plt.savefig("toa_normalized_comparison.pdf")
plt.show()
