[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.614956deg,0.113789deg,0.767534deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.21938mm,-243.655um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.29672deg,-0.647671deg,-0.0541445deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 111.232um,80.837um,24mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.69286deg,-0.586193deg,0.172346deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 410.158um,-351.534um,51mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.210677deg,-1.56824deg,0.137223deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 587.885um,93.213um,149mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.276911deg,-1.34846deg,0.174924deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 585.285um,-56.83um,175mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

