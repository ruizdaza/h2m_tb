[W0013_D04]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 186.02deg,-189.343deg,-0.54557deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 564.343um,249.687um,0um
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_E03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189.444deg,-188.916deg,-0.50426deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -140.182um,173.278um,21mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_G02]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189deg,-189deg,0deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0um,0um,42.5mm
role = "reference"
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[H2M_0]
coordinates = "cartesian"
number_of_pixels = 64, 16
orientation = 179.969deg,0.0191368deg,0.760773deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 375.016um,48.611um,113mm
role = "dut"
spatial_resolution = 10um,10um
time_resolution = 3ns
type = "h2m"

[W0013_G03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.559deg,-8.47324deg,-0.899143deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -35.672um,-226.292um,185.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_J05]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.57deg,-8.54664deg,-0.825403deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -140um,-97.747um,207.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_L09]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.234deg,-8.48562deg,-0.526491deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -194.89um,-160.871um,229mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

