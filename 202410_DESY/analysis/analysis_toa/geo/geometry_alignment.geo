[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.174007deg,180.784deg,-0.441235deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -262.84um,291.259um,8mm
roi = [[260,240],[260,400],[330,400],[330,240]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.246028deg,180.281deg,0.207182deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 439.509um,664.563um,36mm
roi = [[280,220],[280,400],[350,400],[350,220]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -2.11874deg,179.933deg,0.000572958deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -351.2um,438.969um,62mm
roi = [[260,220],[260,400],[320,400],[320,220]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
#mask_file = "/scratch/ruizdaza/h2m/h2m_tb/202410_DESY/analysis/analysis_tot/geo/masking_3V6.txt"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 179.481deg,-0.252331deg,89.9527deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 6.16704mm,2.04155mm,98mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,180deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
roi = [[260,250],[260,410],[330,410],[330,250]]
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.674199deg,179.222deg,0.0181628deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = -38.232um,228.282um,150mm
roi = [[260,250],[260,410],[330,410],[330,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.0443469deg,180.866deg,0.0913295deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 557.75um,48.073um,178mm
roi = [[290,250],[290,410],[350,410],[350,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

