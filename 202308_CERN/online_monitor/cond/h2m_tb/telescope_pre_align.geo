[W0013_D04]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.614deg,-189.201deg,-0.493317deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 536.163um,233.877um,0um
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_E03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189.033deg,-189.274deg,-0.47418deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -145.99um,153.833um,21mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_G02]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189deg,-189deg,0deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0um,0um,42.5mm
role = "reference"
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[H2M_0]
coordinates = "cartesian"
number_of_pixels = 64, 16
orientation = 180deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 0um,0um,113mm
role = "dut"
spatial_resolution = 10um,10um
time_resolution = 3ns
type = "h2m"

[W0013_G03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.614deg,-8.39945deg,-0.954605deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -35.915um,-132.054um,185.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_J05]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.826deg,-8.47542deg,-0.810105deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -140.197um,1.328um,207.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_L09]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.533deg,-8.41566deg,-0.510849deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -185.473um,-69.056um,229mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

