import ROOT
import numpy as np
import matplotlib.pyplot as plt

# Load the ROOT file
file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202405/output_3165/analysis.root"
#file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202404/h2m2_3V6_toa_ikrum21/thr109.root"
root_file = ROOT.TFile.Open(file_path)

# Check if the file is opened successfully
if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

# Navigate to the TProfile path
tprofile_path = "AnalysisDUT/H2M_0/dtvsxmym4p"
tprofile = root_file.Get(tprofile_path)

# Check if the TProfile is found
if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

# Get the TProfile data
nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Create arrays to store x, y, and z values
x_values = np.array([tprofile.GetXaxis().GetBinCenter(i) for i in range(1, nx + 1)])
y_values = np.array([tprofile.GetYaxis().GetBinCenter(j) for j in range(1, ny + 1)])
z_values = np.array([[tprofile.GetBinContent(j, i) for i in range(1, nx + 1)] for j in range(1, ny + 1)])

# Combine the bins
rebin_factor = 10# factor of 2 is 35 bins/pixel. factor of 10 is 7 bins/pixel. factor 7 is 10 bins/pixel

# Function to rebin data by averaging over neighboring bins
def rebin_data(data, factor):
    shape = (data.shape[0] // factor, factor, data.shape[1] // factor, factor)
    return data.reshape(shape).mean(axis=(1, 3))

# Apply rebinning to z_values
z_values_rebinned = rebin_data(z_values, rebin_factor)

# Rebin x and y values accordingly
x_values_rebinned = np.mean(x_values.reshape(-1, rebin_factor), axis=1)
y_values_rebinned = np.mean(y_values.reshape(-1, rebin_factor), axis=1)

# Create a heatmap using matplotlib with rebinned data
plt.figure(figsize=(8, 8))
img = plt.imshow(z_values_rebinned.T, extent=(x_values_rebinned.min(), x_values_rebinned.max(),
                                              y_values_rebinned.min(), y_values_rebinned.max()),
                 origin='lower', cmap='viridis')

# Add colorbar
cbar = plt.colorbar(img, label='$t_{\mathrm{trigger}} - t_{\mathrm{hit}}$ [ns]', shrink=0.82)

# Manually adjust the font size of the colorbar label
cbar.set_label('$t_{\mathrm{trigger}} - t_{\mathrm{hit}}$ [ns]', fontsize=16)

# Set the range of the color map (clim)
#img.set_clim(60, 260)

# Set font size for colorbar tick labels
cbar.ax.tick_params(axis='y', labelsize=16)

# Set axis labels and title with increased label size
plt.xlabel(r'in-pixel $x_{\mathrm{track}}$ [$\mu$m]', fontsize=16)
plt.ylabel(r'in-pixel $y_{\mathrm{track}}$ [$\mu$m]', fontsize=16)

# Add grey discontinuous lines at x=35 and y=35
plt.axvline(x=35, color='grey', linestyle='--')
plt.axhline(y=35, color='grey', linestyle='--')

# Save the plot
output_image_path = "output_timemap_rebinned.pdf"
plt.savefig(output_image_path)

# Show the plot
plt.show()
