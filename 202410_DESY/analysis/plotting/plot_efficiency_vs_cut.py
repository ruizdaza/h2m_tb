import ROOT
import numpy as np

# Define CUT values and pitch
cut_values = [8.75, 17.5, 26.25, 35, 43.75, 52.5, 61.25, 70, 78.75, 87.5]
pitch = 35  # in microns
cut_over_pitch = np.array(cut_values) / pitch

# Arrays to store the values
efficiencies, eff_errors_low, eff_errors_up = [], [], []
fake_rates, fake_errors = [], []

# Loop over ROOT files
for cut in cut_values:
    filename = f"/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/output_918_cut_{cut}um{cut}um/analysis.root"
    file = ROOT.TFile.Open(filename)

    if not file or file.IsZombie():
        print(f"Could not open file: {filename}")
        continue

    # Get efficiency and errors
    efficiency_hist = file.Get("AnalysisEfficiency/H2M_0/eTotalEfficiency")
    if efficiency_hist:
        efficiency = efficiency_hist.GetEfficiency(1)
        efficiency_error_low = efficiency_hist.GetEfficiencyErrorLow(1)
        efficiency_error_up = efficiency_hist.GetEfficiencyErrorUp(1)
        efficiencies.append(efficiency)
        eff_errors_low.append(efficiency_error_low)
        eff_errors_up.append(efficiency_error_up)
    else:
        print(f"No efficiency histogram found in {filename}")


    file.Close()


# plotting
c = ROOT.TCanvas("c", "Efficiency and Fake Hit Rate vs Cut/Pitch", 800, 600)

graph_efficiency = ROOT.TGraphAsymmErrors(len(cut_over_pitch), cut_over_pitch, np.array(efficiencies),
                                          np.zeros(len(cut_over_pitch)), np.zeros(len(cut_over_pitch)),
                                          np.array(eff_errors_low), np.array(eff_errors_up))

graph_efficiency.SetTitle("")
graph_efficiency.GetXaxis().SetTitle("Cut / Pitch")
graph_efficiency.GetYaxis().SetTitle("Efficiency")
graph_efficiency.SetMarkerStyle(20)
graph_efficiency.SetMarkerColor(ROOT.kBlack)
graph_efficiency.SetLineColor(ROOT.kBlack)
graph_efficiency.SetLineWidth(2)
graph_efficiency.GetHistogram().SetMaximum(1.05)
graph_efficiency.GetHistogram().SetMinimum(0)
graph_efficiency.GetXaxis().SetLabelSize(0.05)
graph_efficiency.GetYaxis().SetLabelSize(0.05)
graph_efficiency.GetXaxis().SetTitleSize(0.055)
graph_efficiency.GetYaxis().SetTitleSize(0.055)
graph_efficiency.GetXaxis().SetTitleOffset(0.85)
graph_efficiency.GetYaxis().SetTitleOffset(0.85)
graph_efficiency.Draw("AP")


# Save
c.SaveAs("efficiency_vs_cut_pitch.pdf")
