import ROOT
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.ticker import MaxNLocator

# Load the ROOT file
file_path = "/scratch/ruizdaza/h2m/h2m_tb/202410_DESY/analysis/analysis_tot/output_3175/analysis.root"
#file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202410/h2m8_3V6_ikrum10_tot_thr107.root"

root_file = ROOT.TFile.Open(file_path)

# Check if the file is opened successfully
if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

# Navigate to the TProfile path
tprofile_path = "AnalysisEfficiency/H2M_0/pixelEfficiencyMap4pixels_trackPos_TProfile"
tprofile = root_file.Get(tprofile_path)

# Check if the TProfile is found
if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

tprofile.Rebin2D(35,35)
# Get the TProfile data
nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Create arrays to store x, y, and z values
x_values = np.array([tprofile.GetXaxis().GetBinCenter(i) for i in range(1, nx + 1)])
y_values = np.array([tprofile.GetYaxis().GetBinCenter(j) for j in range(1, ny + 1)])
z_values = np.array([[tprofile.GetBinContent(j, i) for i in range(1, nx + 1)] for j in range(1, ny + 1)])

# Calculate projections in x and y directions
x_projection = np.mean(z_values, axis=0)
y_projection = np.mean(z_values, axis=1)

# Create a figure with subplots using gridspec
fig = plt.figure(figsize=(10, 10))
gs = gridspec.GridSpec(3, 3, width_ratios=[1, 4, 0.3], height_ratios=[1, 4, 0.3], wspace=0.35, hspace=0.35)

# Main heatmap
ax_heatmap = plt.subplot(gs[1, 1])
img = ax_heatmap.imshow(z_values.T, extent=(x_values.min(), x_values.max(), y_values.min(), y_values.max()), origin='lower', cmap='viridis', vmin=0.7, vmax=1)
ax_heatmap.axvline(x=35, color='grey', linestyle='--')
ax_heatmap.axhline(y=35, color='grey', linestyle='--')
ax_heatmap.set_xlabel(r'in-pixel $x_{\mathrm{track}}$ [$\mu$m]', fontsize=16)
ax_heatmap.set_ylabel(r'in-pixel $y_{\mathrm{track}}$ [$\mu$m]', fontsize=16)

# Colorbar
cbar_ax = plt.subplot(gs[1, 2])
cbar = plt.colorbar(img, cax=cbar_ax)
cbar.set_label('Efficiency', fontsize=16)
cbar.ax.tick_params(axis='y', labelsize=16)

# X projection
ax_xproj = plt.subplot(gs[0, 1], sharex=ax_heatmap)
ax_xproj.plot(y_values, y_projection, 'k-')
ax_xproj.set_ylabel('Mean Efficiency', fontsize=12)
ax_xproj.tick_params(axis='x', labelbottom=False)
ax_xproj.axvline(x=35, color='grey', linestyle='--')
ax_xproj.set_xlim(x_values.min(), x_values.max())
ax_xproj.set_ylim(0.75, 1)  # Set the y-axis range for the x projection
ax_xproj.yaxis.set_major_locator(MaxNLocator(nbins=3))  # Set the number of ticks

# Highlight specific bands in x projection
ax_xproj.axvspan(15, 30, color='blue', alpha=0.1)
ax_xproj.axvspan(50, 65, color='blue', alpha=0.1)
ax_xproj.axvspan(x_values.min(), 15, color='green', alpha=0.1)
ax_xproj.axvspan(30, 50, color='green', alpha=0.1)
ax_xproj.axvspan(65, x_values.max(), color='green', alpha=0.1)

# Y projection
ax_yproj = plt.subplot(gs[1, 0], sharey=ax_heatmap)
ax_yproj.plot(x_projection, x_values, 'k-')
ax_yproj.set_xlabel('Mean Efficiency', fontsize=12)
ax_yproj.tick_params(axis='y', labelleft=False)
ax_yproj.axhline(y=35, color='grey', linestyle='--')
ax_yproj.set_ylim(y_values.min(), y_values.max())
ax_yproj.set_xlim(0.75, 1)  # Set the x-axis range for the y projection
ax_yproj.invert_xaxis()  # Invert the x-axis of the y projection
ax_yproj.xaxis.set_major_locator(MaxNLocator(nbins=3))  # Set the number of ticks


# Save the plot
output_image_path = "output_effmap_matplotlib_with_projections_202405_3175.pdf"
plt.savefig(output_image_path)

# Show the plot
plt.show()
