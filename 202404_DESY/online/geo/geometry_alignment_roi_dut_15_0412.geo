[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.47158deg,0.590834deg,0.774009deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.32174mm,-214.362um,0um
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.44368deg,-1.23564deg,-0.0511078deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 153.508um,91.429um,24mm
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.77308deg,-0.890319deg,0.175726deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 353.006um,-371.601um,51mm
roi = [[350,250],[350,380],[550,380],[550,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.0379871deg,0.0357526deg,-0.822137deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -730.222um,1.23861mm,96mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
roi = [[350,250],[350,380],[580,380],[580,250]]
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.23157deg,-0.467419deg,0.137854deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 592.972um,95.921um,149mm
roi = [[350,250],[350,380],[580,380],[580,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.45228deg,0.577943deg,0.18306deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 537.995um,-81.725um,175mm
roi = [[350,250],[350,380],[580,380],[580,250]]
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

