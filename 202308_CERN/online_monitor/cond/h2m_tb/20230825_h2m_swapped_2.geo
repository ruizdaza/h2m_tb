[W0013_D04]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.669deg,-189.184deg,-0.508844deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 565.465um,226.137um,0um
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_E03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189.066deg,-189.259deg,-0.485009deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -141.459um,163.504um,21mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_G02]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 189deg,-189deg,0deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = 0um,0um,42.5mm
role = "reference"
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[H2M_0]
coordinates = "cartesian"
number_of_pixels = 64, 16
orientation = -0.0268717deg,0.0229756deg,0.789708deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = 424.615um,34.483um,113mm
role = "dut"
spatial_resolution = 10um,10um
time_resolution = 3ns
type = "h2m"

[W0013_G03]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 188.699deg,-8.43686deg,-0.92441deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -29.364um,-144.7um,185.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_J05]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.906deg,-8.51971deg,-0.818814deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -133.282um,-6.13um,207.5mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

[W0013_L09]
coordinates = "cartesian"
material_budget = 0.01068
number_of_pixels = 256, 256
orientation = 187.634deg,-8.478deg,-0.525116deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -187.279um,-64.704um,229mm
spatial_resolution = 2.7um,2.7um
time_resolution = 1.5625ns
type = "timepix3"

