import uproot
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

# Open ROOT files
file_tuned = uproot.open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202405/output_3155/analysis.root")
file_small = uproot.open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202405/output_3178/analysis.root")


# Get histograms
h_tuned = file_tuned["AnalysisDUT/H2M_0/residualsTime"]
h_small = file_small["AnalysisDUT/H2M_0/residualsTime"]


#h_tuned = file_tuned["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]
#h_small = file_small["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]
#h_large = file_large["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]
#h_new = file_new["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]


# Convert uproot histograms to NumPy arrays
values_tuned, bin_edges_tuned = h_tuned.to_numpy()
values_small, bin_edges_small = h_small.to_numpy()


# Calculate bin centers
bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
bin_centers_small = (bin_edges_small[:-1] + bin_edges_small[1:]) / 2


# Normalize histograms.  The normalization is done to the total area under the histogram, making the integral of each histogram equal to 1.
norm_tuned = np.sum(values_tuned) * (bin_centers_tuned[1] - bin_centers_tuned[0])
norm_small = np.sum(values_small) * (bin_centers_small[1] - bin_centers_small[0])


# Create a plot using matplotlib
plt.figure(figsize=(8, 6)) # Modify for different desired figsize


# Plot histograms
plt.plot(bin_centers_tuned, values_tuned/norm_tuned, color='red', label='-1.2 V', drawstyle='steps-mid', zorder=2, linewidth=1, alpha = 0.8)
plt.plot(bin_centers_small, values_small/norm_small, color='green', label='-3.6 V', drawstyle='steps-mid', zorder=2, linewidth=1, alpha = 0.5)
#plt.plot(bin_centers_large, values_large/norm_large, color='black', label='ikrum = 10, thr = 320 estimated e-', drawstyle='steps-mid', zorder=2, linewidth=2)
#plt.plot(bin_centers_new, values_new/norm_new, color='blue', label='ikrum = 21, thr = 320 estimated e-', drawstyle='steps-mid', zorder=2, linewidth=2)

# Fill the area under the curves
#plt.fill_between(bin_centers_tuned, 0, values_tuned/norm_tuned, color='seagreen', alpha=0.5, zorder=1, step='mid')
#plt.fill_between(bin_centers_small, 0, values_small/norm_small, color='darkmagenta', alpha=0.5, zorder=1, step='mid')
#plt.fill_between(bin_centers_large, 0, values_large/norm_large, color='red', alpha=1, zorder=0.5, step='mid')

# Set axis labels
plt.xlabel('$t_{trigger} - t_{hit}$ [ns]', fontsize=16)
plt.ylabel('Normalised frequency', fontsize=16)

# Set axis limits
plt.ylim(0, 0.05)
plt.xlim(-100, 250)

# Add legend
plt.legend(fontsize=16, frameon=False)

# Save the plot
output_pdf_path = "toa_distribution_bias.pdf"
plt.savefig(output_pdf_path)

# Show the plot
plt.show()

# Close the ROOT files
file_tuned.close()
file_small.close()

print(f"Plot saved as {output_pdf_path}")
