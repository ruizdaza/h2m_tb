import ROOT
import numpy as np
import matplotlib.pyplot as plt

# Load the ROOT file
file_path = "/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202405/output_3155/analysis.root"
root_file = ROOT.TFile.Open(file_path)

# Alert if the root file is not found
if not root_file or root_file.IsZombie():
    print(f"Error: Unable to open ROOT file '{file_path}'")
    exit()

# Open the plot
tprofile_path = "AnalysisDUT/H2M_0/timeDelay_trackPos_TProfile"
tprofile = root_file.Get(tprofile_path)

# Alert if the plot is not found
if not tprofile:
    print(f"Error: TProfile {tprofile_path} not found in the ROOT file.")
    exit()

# Get the number of bins in X and Y
nx = tprofile.GetNbinsX()
ny = tprofile.GetNbinsY()

# Check that we get 140 bins in X and in Y because it is the 4 pixel plot
print('bins in X: ', nx, ' ; bins in Y: ', ny)

# Function to extract cluster charge values for the specified square region
def extract_cluster_charge_values(start_bin_x, end_bin_x, start_bin_y, end_bin_y):
    values = []
    for i in range(start_bin_x, end_bin_x + 1):
        for j in range(start_bin_y, end_bin_y + 1):  # bins start from 1
            bin_content = tprofile.GetBinContent(i, j)
            bin_entries = int(tprofile.GetBinEntries(tprofile.GetBin(i, j)))
            values.extend([bin_content] * bin_entries)
    return values

# Extract cluster charge values for the specified square region
start_bin_x = 15*2
end_bin_x = 19*2
start_bin_y = 15*2
end_bin_y = 19*2

cluster_charge_values_square = extract_cluster_charge_values(start_bin_x, end_bin_x, start_bin_y, end_bin_y)

# Shift values to make them non-negative
min_value = np.min(cluster_charge_values_square)
shift = -min_value if min_value < 0 else 0

# Calculate the raw bin counts (without normalization)
if cluster_charge_values_square:
    shifted_values_square = np.array(cluster_charge_values_square) + shift
    bin_counts_square = np.bincount(shifted_values_square.astype(int))
else:
    bin_counts_square = np.array([])

# Aggregate bin counts in groups of 2
def aggregate_bin_counts(bin_counts, group_size):
    aggregated_counts = np.add.reduceat(bin_counts, np.arange(0, len(bin_counts), group_size))
    return aggregated_counts

group_size = 1
aggregated_counts_square = aggregate_bin_counts(bin_counts_square, group_size)

# Calculate total number of events in the square region
total_events_square = len(cluster_charge_values_square)

# Calculate MPV for the square region
def calculate_mpv(bin_counts, shift, group_size):
    if bin_counts.size > 0:
        max_index = np.argmax(bin_counts)
        mpv = (max_index * group_size) - shift  # Adjust MPV for binning
    else:
        mpv = None
    return mpv

mpv_square = calculate_mpv(aggregated_counts_square, shift, group_size)

# Calculate RMS for the square region using weighted average method
def calculate_rms_weighted(bin_counts, shift, group_size):
    if bin_counts.size == 0:
        return None

    bin_centers = np.arange(len(bin_counts)) * group_size - shift
    values = bin_counts / np.sum(bin_counts)

    mean = np.average(bin_centers, weights=values)
    variance = np.average((bin_centers - mean) ** 2, weights=values)
    rms = np.sqrt(variance)

    return rms

rms_square = calculate_rms_weighted(aggregated_counts_square, shift, group_size)

# Plot raw bin counts (without normalization)
plt.figure(figsize=(8, 6))

if aggregated_counts_square.size > 0:
    x_values = np.arange(len(aggregated_counts_square)) * group_size - shift
    plt.bar(x_values, aggregated_counts_square, width=group_size, label=f'25 $\mu$m$^2$, Events: {total_events_square}, MPV: {mpv_square}, RMS: {rms_square:.2f}', color='red', alpha=0.5)

plt.xlabel(r'$t_{\mathrm{trigger}} - t_{\mathrm{hit}}$ [ns]', fontsize=16)
plt.ylabel('Number of events', fontsize=16)
plt.xlim(-100, 160)  # Set the limit from -100 to 160 in the x-axis
plt.legend(frameon=False)

plt.tight_layout()

# Save the plot as PDF
plt.savefig("toa_collection_electrode.pdf")
plt.show()
