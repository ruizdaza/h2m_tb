#!/bin/bash

RUN=${1}

datapath=/pnfs/desy.de/ftx-tb/202404_H2M/data


INPUTFILE_H2M=$(find $datapath -name "run*0${RUN}_h2m_*" )
INPUTFILE_TL=$(find $datapath -name "run*0${RUN}_tel_*" )
INPUTFILE_TLU=$(find $datapath -name "run*0${RUN}_tel_*" )


echo "--- Input H2M: "  ${INPUTFILE_H2M}
echo "--- Input telescope: " ${INPUTFILE_TL}
echo "--- Input TLU: " ${INPUTFILE_TLU}


#/scratch/ruizdaza/corryvreckan/bin/corry -c 2_prealignment.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
#/scratch/ruizdaza/corryvreckan/bin/corry -c 3_tracking_after_prealignment.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
#/scratch/ruizdaza/corryvreckan/bin/corry -c 4_alignment.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
#/scratch/ruizdaza/corryvreckan/bin/corry -c 5_first_dut_residuals.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 6_dut_alignment_1.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 6_dut_alignment_2.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 6_dut_alignment_3.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
/scratch/ruizdaza/corryvreckan/bin/corry -c 7_dut_analysis.conf -o EventLoaderEUDAQ2:H2M_0.file_name=${INPUTFILE_H2M} -o EventLoaderEUDAQ2:TLU_0.file_name=${INPUTFILE_TLU} -o EventLoaderEUDAQ2.file_name=${INPUTFILE_TL} &&
cp -r output /pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202404/output_${RUN} && cp -r geo /pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_desy_202404/geo_${RUN}
#cp -r output output_${RUN} && cp -r geo geo_${RUN}
