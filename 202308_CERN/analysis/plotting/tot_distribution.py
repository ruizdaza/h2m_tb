import uproot
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

# Open ROOT files
file_tuned = uproot.open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_130205/dut_analysis.root")
file_small = uproot.open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_130208/dut_analysis.root")
file_large = uproot.open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_130209/dut_analysis.root")
file_new = uproot.open("/pnfs/desy.de/ftx-tb/ruizdaza/h2m/tb_cern/output_130206/dut_analysis.root")


# Get histograms
h_tuned = file_tuned["AnalysisDUT/H2M_0/pixelRawValueAssoc"]
h_small = file_small["AnalysisDUT/H2M_0/pixelRawValueAssoc"]
h_large = file_large["AnalysisDUT/H2M_0/pixelRawValueAssoc"]
h_new = file_new["AnalysisDUT/H2M_0/pixelRawValueAssoc"]

#h_tuned = file_tuned["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]
#h_small = file_small["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]
#h_large = file_large["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]
#h_new = file_new["EventLoaderEUDAQ2/H2M_0/hPixelRawValues"]


# Convert uproot histograms to NumPy arrays
values_tuned, bin_edges_tuned = h_tuned.to_numpy()
values_small, bin_edges_small = h_small.to_numpy()
values_large, bin_edges_large = h_large.to_numpy()
values_new, bin_edges_new = h_new.to_numpy()

# Calculate bin centers
bin_centers_tuned = (bin_edges_tuned[:-1] + bin_edges_tuned[1:]) / 2
bin_centers_small = (bin_edges_small[:-1] + bin_edges_small[1:]) / 2
bin_centers_large = (bin_edges_large[:-1] + bin_edges_large[1:]) / 2
bin_centers_new = (bin_edges_new[:-1] + bin_edges_new[1:]) / 2

# Normalize histograms.  The normalization is done to the total area under the histogram, making the integral of each histogram equal to 1.
norm_tuned = np.sum(values_tuned) * (bin_centers_tuned[1] - bin_centers_tuned[0])
norm_small = np.sum(values_small) * (bin_centers_small[1] - bin_centers_small[0])
norm_large = np.sum(values_large) * (bin_centers_large[1] - bin_centers_large[0])
norm_new = np.sum(values_new) * (bin_centers_new[1] - bin_centers_new[0])

# Create a plot using matplotlib
plt.figure(figsize=(8, 6)) # Modify for different desired figsize

# Plot histograms
plt.plot(bin_centers_tuned, values_tuned/norm_tuned, color='green', label='ikrum = 38', drawstyle='steps-mid', zorder=2, linewidth=2)
plt.plot(bin_centers_new, values_new/norm_new, color='blue', label='ikrum = 32', drawstyle='steps-mid', zorder=2, linewidth=2)
plt.plot(bin_centers_small, values_small/norm_small, color='red', label='ikrum = 21', drawstyle='steps-mid', zorder=2, linewidth=2)
plt.plot(bin_centers_large, values_large/norm_large, color='black', label='ikrum = 19', drawstyle='steps-mid', zorder=2, linewidth=2)

# Fill the area under the curves
#plt.fill_between(bin_centers_tuned, 0, values_tuned/norm_tuned, color='seagreen', alpha=0.5, zorder=1, step='mid')
#plt.fill_between(bin_centers_small, 0, values_small/norm_small, color='darkmagenta', alpha=0.5, zorder=1, step='mid')
#plt.fill_between(bin_centers_large, 0, values_large/norm_large, color='red', alpha=1, zorder=0.5, step='mid')

# Set axis labels
plt.xlabel('ToT [clock cycles]', fontsize=16)
plt.ylabel('Normalised frequency', fontsize=16)

# Set axis limits
plt.ylim(0, 0.09)
plt.xlim(0, 100)

# Add legend
plt.legend(fontsize=16, frameon=False)

# Save the plot
output_pdf_path = "tot_distribution.pdf"
plt.savefig(output_pdf_path)

# Show the plot
plt.show()

# Close the ROOT files
file_tuned.close()
file_small.close()
file_large.close()
file_new.close()
print(f"Plot saved as {output_pdf_path}")
