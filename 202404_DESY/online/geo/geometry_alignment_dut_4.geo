[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 1.19026deg,0.658386deg,0.772404deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.21926mm,-243.531um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.3806deg,-1.07647deg,-0.0524829deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 111.001um,80.838um,24mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.79886deg,-0.735162deg,0.174007deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 410.02um,-351.404um,51mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = -0.0528267deg,-0.485926deg,-0.846602deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -717.474um,1.24654mm,96mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,123mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.796182deg,-0.297365deg,0.13751deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 588.139um,93.441um,149mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.990759deg,0.169939deg,0.177789deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 585.151um,-56.616um,175mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

