[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.00784952deg,0.0276166deg,0.795552deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.37741mm,61.977um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.820361deg,-0.551185deg,-0.0638848deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 190.778um,435.788um,105mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.96335deg,-0.427255deg,0.167647deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 604.471um,39.363um,132mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.274103deg,0.275593deg,0.186842deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -733.437um,210.748um,172mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,194mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.646927deg,-0.444673deg,0.143469deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 719.796um,90.225um,221mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.1767deg,-1.69492deg,0.150115deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.40134mm,-71.906um,325mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

