[adenium_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0.111612deg,0.0067609deg,0.796354deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.37697mm,61.686um,0um
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[TLU_0]
coordinates = "cartesian"
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
position = 0um,0um,16.2mm
role = "auxiliary"
time_resolution = 1ns
type = "tlu"

[adenium_1]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -1.26051deg,-0.356666deg,-0.0638275deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 190.709um,435.434um,105mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_2]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -2.25751deg,-0.441521deg,0.168106deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 604.51um,39.276um,132mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[H2M_0]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 64, 16
orientation = 0.0121467deg,0.0729948deg,0.256914deg
orientation_mode = "xyz"
pixel_pitch = 35um,35um
position = -767.762um,193.906um,172mm
role = "dut"
spatial_resolution = 9um,9um
time_resolution = 1ns
type = "h2m"

[adenium_3]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = 0deg,0deg,0deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 0um,0um,194mm
role = "reference"
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_4]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.865166deg,-0.384569deg,0.143182deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 719.728um,89.651um,221mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[adenium_5]
coordinates = "cartesian"
material_budget = 0.00075
number_of_pixels = 1024, 512
orientation = -0.624352deg,-1.49731deg,0.15361deg
orientation_mode = "xyz"
pixel_pitch = 29.24um,26.88um
position = 1.40136mm,-72.835um,325mm
spatial_resolution = 5um,5um
time_resolution = 10us
type = "adeniumrawdataevent"

[mp10_0]
coordinates = "cartesian"
material_budget = 0.00375
number_of_pixels = 120, 400
orientation = 179.476deg,1.02663deg,-1.68054deg
orientation_mode = "xyz"
pixel_pitch = 165um,25um
position = 2.41318mm,952.701um,386mm
spatial_resolution = 43um,14um
time_resolution = 200ns
time_offset = -216ns
type = "telepix2"

